""" This file defines the base algorithm class. """

import abc
import copy
import logging
import random
import numpy as np
import scipy as sp
from scipy import linalg
from algorithm_utils import IterationData, TrajectoryInfo, PolicyInfo, quadratic_regression
from traj_opt_lqr_python import TrajOptLQRPython
import policy_prior
from lin_gauss_policy import LinearGaussianPolicy
from IPython import embed
from dynamics_lr import DynamicsLR


OC_OPTIONS = [
    ("kl_step", float, 0.1, "Per-step KL constraint for Optimal Control"),
    ("initial_state_var", float, 0.001, "Initial State Variance for OC"),
    ("history_k", int, 10, "use all the data from the most recent k iterations to fit dynamics,costs,NN-policy"),
    ("learn_cost", int, 0, "whether or not use quadratic regression to fit cost functions"),
]


class Algorithm(object):

    def __init__(self, env, ob_space, ac_space, parameters):
        #init_traj_distr: in our case, it is the linearized NN policies in the format of lin_gausss_policy.
        self.T = int(parameters["T"])
        self.dX = ob_space.shape[0]
        self.dU = ac_space.shape[0]

        self.kl_step = parameters["kl_step"]
        self.initial_state_var = parameters["initial_state_var"]
        self.gamma = parameters["gamma"]
        self.history_k = parameters["history_k"]
        self.cost_learn_flag = parameters["learn_cost"]

        self.env = env #env contains cost function and Hessian and other information. 
        self.agent = None
        #policy tools used for learning linear NN policy and store information
        self.policy_prior = policy_prior.PolicyPrior()
        
        # IterationData objects for each condition.
        self.cur = IterationData() #used to store the most recently computed linear-gauss policies.
        self.cur.traj_info = TrajectoryInfo() #stors linear guass dynamics, initial belief, and costs functions parameters. 
        self.cur.traj_info.dynamics = DynamicsLR() #dynamicsLR
        
        #initialize the linear policy:
        self.cur.traj_distr = LinearGaussianPolicy(
                K =np.zeros((self.T,self.dU,self.dX)), 
                k=np.zeros((self.T, self.dU)), 
                pol_covar= np.array([np.eye(self.dU)*self.initial_state_var]*self.T),  #np.zeros((self.T,self.dU,self.dU)), 
                chol_pol_covar= np.array([sp.linalg.cholesky(np.eye(self.dU)*self.initial_state_var)]*self.T),   #np.zeros((self.T,self.dU,self.dU)), 
                inv_pol_covar= np.array([np.eye(self.dU)*1./self.initial_state_var]*self.T))  #np.zeros((self.T,self.dU,self.dU)))
        
        self.traj_opt = TrajOptLQRPython()

        self.memory_X = []
        self.memory_X_raw = []
        self.memory_U = []
        self.memory_C = []

    def append_to_memory(self, X, X_raw, U, C):
        self.memory_X.append(X)
        self.memory_X_raw.append(X_raw)
        self.memory_U.append(U)
        self.memory_C.append(C)
        if len(self.memory_X) > self.history_k:
            self.memory_X.pop(0) #pop the old ones. 
            self.memory_X_raw.pop(0)
            self.memory_U.pop(0) 
            self.memory_C.pop(0)


    def iteration(self, agent):
        #X: NxTxdX, U: NxTxdU
        #self.memory_X.append(X)
        #self.memory_U.append(U)
        #if len(self.memory_X) > self.history_k:
        #    self.memory_X.pop(0) #pop the old ones. 
        #    self.memory_U.pop(0) 

        self.agent = agent #update agent.
        self._update_dynamics(np.concatenate(self.memory_X_raw,axis=0),
                    np.concatenate(self.memory_U,axis=0)) #fit dynamics on the new data, stored in cur.traj_info.dyanmics

        if self.cost_learn_flag == 0:
            self._eval_cost(
                    np.concatenate(self.memory_X_raw,axis=0), 
                    np.concatenate(self.memory_U,axis=0),
                    np.concatenate(self.memory_C,axis=0)) #fit cost function on the new data, stored in cur.traj_info
        elif self.cost_learn_flag == 1:
            self._learn_cost(
                    np.concatenate(self.memory_X_raw,axis=0), 
                    np.concatenate(self.memory_U,axis=0),
                    np.concatenate(self.memory_C,axis=0))

        self._update_policy_fit(np.concatenate(self.memory_X,axis=0),
                               np.concatenate(self.memory_X_raw,axis=0)) #linearly fit  NN policy on new data, stored in nn_traj_distr

        self.new_traj_distr, self.cur.eta = self.traj_opt.update(self) #new_traj_distr: the converged linear-gauss-policy.
        self.cur.traj_distr = self.new_traj_distr #update the cur traj policy to the newly computed linear-Gauss.
        
        #backward again to compute CTG and VTG:
        self.Qtt, self.Qt, self.Vtt, self.Vt = self.traj_opt.backward_wth_policy(
                self.cur.traj_distr, self.cur.traj_info, self)


    def _update_dynamics(self, X, U):
        """
        X: NxTxdX  N:number of trajectories. 
        U: NXTxdU  N:number of trajectories
        Instantiate dynamics objects and update prior. Fit dynamics to
        current samples.
        """
        # Update prior and fit dynamics.
        self.cur.traj_info.dynamics.fit(X, U)
        # Fit x0mu/x0sigma.
        x0 = X[:, 0, :]
        x0mu = np.mean(x0, axis=0)
        self.cur.traj_info.x0mu = x0mu
        self.cur.traj_info.x0sigma = np.cov(x0.T) + 1e-8*np.eye(x0mu.shape[0])
        #embed()
        #self.cur.traj_info.x0sigma = np.diag(np.maximum(np.var(x0, axis=0), self.initial_state_var))


    def _update_policy_fit(self, X, X_raw):
        """
        Re-estimate the local policy values in the neighborhood of the
        trajectory.
        Args:
            X: NxTxdX
        """
        assert X.ndim == 3
        dX, dU, T, N = self.dX, self.dU, self.T, X.shape[0]
        assert X.shape[0] == N and X.shape[1] == T and X.shape[2] == dX

        pol_m, pol_sig = self.agent.policy.get_mean_vars(X.reshape(N*T,-1))
        pol_m = pol_m.reshape(N,T,-1) #mean  
        pol_sig = pol_sig.reshape(N,T,-1) #diag-variances

        #linearize the policy around X, pol_mu, and add pol_sig.
        pol_K, pol_k, pol_S = self.policy_prior.fit(X_raw, pol_m, pol_sig)
        chol_pol_S = np.zeros((T, self.dU, self.dU))
        inv_pol_S = np.zeros((T,self.dU, self.dU))

        for t in range(T):
            chol_pol_S[t, :, :] = sp.linalg.cholesky(pol_S[t, :, :])
            inv_pol_S[t,:,:] = sp.linalg.solve_triangular(chol_pol_S[t,:,:],
                    sp.linalg.solve_triangular(chol_pol_S[t,:,:].T, np.eye(self.dU),lower=True)
            )

        self.nn_traj_distr = LinearGaussianPolicy(K = pol_K, 
                        k = pol_k, pol_covar = pol_S,
                        chol_pol_covar = chol_pol_S, 
                        inv_pol_covar = inv_pol_S)


    def _learn_cost(self, X, U, C):
        """
        Learn time-dependent quadratic cost functions. 
        Args:
            X: NxTxdX
            U: NxTxdU
            C: N*T
        """
        assert X.ndim == 3 and U.ndim == 3
        T, dX, dU = self.T, self.dX, self.dU
        N = X.shape[0]
        assert dX == X.shape[2] and dU == U.shape[2]

        # Compute cost.
        cc = np.zeros(T)
        cv = np.zeros((T, dX+dU))
        Cm = np.zeros((T, dX+dU, dX+dU))

        for t in xrange(T):
            X_t = X[:, t, :]
            U_t = U[:, t, :]
            C_t = C[:, t]
            XU_t = np.c_[X_t, U_t]
            Q,l,c = quadratic_regression(XU_t, C_t.reshape(N,1))
            Cm[t] = Q
            cv[t] = l
            cc[t] = c
    
        self.cur.traj_info.cc = cc
        self.cur.traj_info.cv = cv
        self.cur.traj_info.Cm = Cm
        self.cur.cs = C


    def _eval_cost(self, X, U, C=None):
        """
        Evaluate costs for all samples for a condition.
        Args:
            X: NxTxdX
            U: NxTxdU
            C: N*T
        """
        # Constants.
        assert X.ndim == 3 and U.ndim == 3
        T, dX, dU = self.T, self.dX, self.dU
        N = X.shape[0]
        assert dX == X.shape[2] and dU == U.shape[2]

        # Compute cost.
        cs = np.zeros((N, T))
        cc = np.zeros((N, T))
        cv = np.zeros((N, T, dX+dU))
        Cm = np.zeros((N, T, dX+dU, dX+dU))
        for n in range(N):
            # Get costs.
            l, lx, lu, lxx, luu, lux = self.env.eval(X[n], U[n], C[n])   #eval: return the Hessian, gradient. 
            #turn graident and hessian into a quadratic format: rearrange consts. 
            cc[n, :] = l #l: T, lx:TxdX, lu:TxdU, luu:TxdUxdU, lxx:TxdXxdX
            cs[n, :] = l
            
            # Assemble matrix and vector.
            cv[n, :, :] = np.c_[lx, lu]
            Cm[n, :, :, :] = np.concatenate(
                (np.c_[lxx, np.transpose(lux, [0, 2, 1])], np.c_[lux, luu]),
                axis=1
            ) #Hessian for [x,u]

            # Adjust for expanding cost around a sample.
            Xn = X[n]
            Un = U[n]
            yhat = np.c_[Xn, Un]
            rdiff = -yhat
            rdiff_expand = np.expand_dims(rdiff, axis=2)
            cv_update = np.sum(Cm[n, :, :, :] * rdiff_expand, axis=1)
            cc[n, :] += np.sum(rdiff * cv[n, :, :], axis=1) + 0.5 * \
                    np.sum(rdiff * cv_update, axis=1)
            cv[n, :, :] += cv_update

        # Fill in cost estimate.
        self.cur.traj_info.cc = np.mean(cc, 0)  # Constant term (scalar).
        self.cur.traj_info.cv = np.mean(cv, 0)  # Linear term (vector).
        self.cur.traj_info.Cm = np.mean(Cm, 0)  # Quadratic term (matrix).
        self.cur.cs = cs  # True value of cost.

    '''
    def _advance_iteration_variables(self):
        """
        Move all 'cur' variables to 'prev', and advance iteration
        counter.
        """
        self.iteration_count += 1
        self.prev = copy.deepcopy(self.cur)
        # TODO: change IterationData to reflect new stuff better
        self.prev.new_traj_distr = self.new_traj_distr
        self.cur = IterationData()
    
        self.cur.traj_info = TrajectoryInfo()
        self.cur.traj_info.dynamics = copy.deepcopy(self.prev.traj_info.dynamics)
        self.cur[m].eta = self.prev[m].eta
        self.cur[m].traj_distr = self.new_traj_distr[m]   
        delattr(self, 'new_traj_distr')
    '''

    def compute_costs(self, eta, augment=True, NN = True):
        """ Compute cost estimates used in the LQR backward pass. """
        #traj_info stores dynamics, quadratic cost parameters, and initial belief.
        #nn_traj_distr stores linearized NN policies. 
        traj_info, traj_distr = self.cur.traj_info, self.cur.traj_distr
        if not augment:  # Whether to augment cost with term to penalize KL
            return traj_info.Cm, traj_info.cv

        T, dU, dX = traj_distr.T, traj_distr.dU, traj_distr.dX
        Cm, cv = np.copy(traj_info.Cm), np.copy(traj_info.cv)

        PKLm = np.zeros((T, dX+dU, dX+dU))
        PKLv = np.zeros((T, dX+dU))
        fCm, fcv = np.zeros(Cm.shape), np.zeros(cv.shape)
        for t in range(T):
            # Policy KL-divergence terms.
            if NN is True:
                inv_pol_S = self.nn_traj_distr.inv_pol_covar[t]
                KB, kB = self.nn_traj_distr.K[t], self.nn_traj_distr.k[t]
            else:
                inv_pol_S = self.cur.traj_distr.inv_pol_covar[t]
                KB,kB = self.cur.traj_distr.K[t], self.cur.traj_distr.k[t]

            PKLm[t, :, :] = np.vstack([
                np.hstack([KB.T.dot(inv_pol_S).dot(KB), -KB.T.dot(inv_pol_S)]),
                np.hstack([-inv_pol_S.dot(KB), inv_pol_S])
            ])
            PKLv[t, :] = np.concatenate([
                KB.T.dot(inv_pol_S).dot(kB), -inv_pol_S.dot(kB)
            ])
            fCm[t, :, :] = Cm[t, :, :]/eta + PKLm[t, :, :]
            fcv[t, :] = cv[t, :]/eta + PKLv[t, :]
            
        return fCm, fcv 

