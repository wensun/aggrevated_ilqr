""" This file defines code for iLQG-based trajectory optimization. """
###this is modified from the GPS code
import logging
import copy
import numpy as np
from numpy.linalg import LinAlgError
import scipy as sp
from scipy import linalg
from traj_opt_utils import traj_distr_kl
from IPython import embed

class TrajOptLQRPython(object):
    """ LQR trajectory optimization, Python implementation. """
    def __init__(self):
        self.min_eta = 1e-8
        self.max_eta = 1e16
        self.DGD_MAX_ITER = 50

    def update(self, algorithm, NN = True):
        """ Run dual gradient decent to optimize trajectories. """
        T = algorithm.T
        eta = algorithm.cur.eta 
        
        traj_info = algorithm.cur.traj_info
        if NN is True:
            prev_traj_distr = algorithm.nn_traj_distr #linearized NN policy in the format of lin_gauss_policy.
        else:
            prev_traj_distr = algorithm.cur.traj_distr #use the most recent linear-gauss

        # Set KL-divergence step size (epsilon).
        kl_step = T*algorithm.kl_step #total kl along all T steps
        
        # We assume at min_eta, kl_div > kl_step, opposite for max_eta.
        min_eta = self.min_eta
        max_eta = self.max_eta
    
        max_itr =  self.DGD_MAX_ITER 
        for itr in range(max_itr):

            # Run fwd/bwd pass, note that eta may be updated.
            # Compute KL divergence constraint violation.
            traj_distr, eta = self.backward(prev_traj_distr, traj_info,eta, algorithm,NN)
            new_mu, new_sigma = self.forward(traj_distr, traj_info)
            
            #E_{s~p_new}[KL(pi_new || pi_prev)]
            kl_div = traj_distr_kl(
                    new_mu, new_sigma, traj_distr, prev_traj_distr,tot=True #times T already
            )
            con = kl_div - kl_step

            # Convergence check - constraint satisfaction.
            if self._conv_check(con, kl_step): #succeeded if the change is less than 10% of the kl_step: 
                print ("KL: {} / {}, converged iteration {}".format(kl_div, kl_step, itr))
                break #exit the iteration loop.

            # Choose new eta (bisect bracket or multiply by constant)
            if con < 0: # Eta was too big.
                max_eta = eta
                geom = np.sqrt(min_eta*max_eta)  # Geometric mean.
                new_eta = max(geom, 0.1*max_eta)
                print("KL: {} / {}, eta too big, new eta: {}; min_eta {}, max_eta {}".format(
                    kl_div, kl_step, new_eta,min_eta,max_eta))
            else: # Eta was too small.
                min_eta = eta
                geom = np.sqrt(min_eta*max_eta)  # Geometric mean.
                new_eta = min(geom, 10.0*min_eta)
                print("KL: {} / {}, eta too small, new eta: {}; min_eta {}, max_eta {}".format(
                    kl_div, kl_step, new_eta, min_eta, max_eta))

            eta = new_eta

        if (np.mean(kl_div) > np.mean(kl_step) and
            not self._conv_check(con, kl_step)):
            print(
                    "Final KL divergence after DGD convergence is too high."
            )
        return traj_distr, eta


    def forward(self, traj_distr, traj_info):
        """
        Perform LQR forward pass. Computes state-action marginals from
        dynamics and policy.
        Args:
            traj_distr: A linear Gaussian policy object.
            traj_info: A TrajectoryInfo object.
        Returns:
            mu: A T x dX mean action vector.
            sigma: A T x dX x dX covariance matrix.
        """
        # Compute state-action marginals from specified conditional
        # parameters and current traj_info.
        T = traj_distr.T
        dU = traj_distr.dU
        dX = traj_distr.dX

        # Constants.
        idx_x = slice(dX)

        # Allocate space.
        sigma = np.zeros((T, dX+dU, dX+dU))
        mu = np.zeros((T, dX+dU))

        # Pull out dynamics.  x_{t+1} = Fm[t][x_t,u_t]^T + fv[t] + e, e~N(0, dyn_covar[t])
        Fm = traj_info.dynamics.Fm
        fv = traj_info.dynamics.fv
        dyn_covar = traj_info.dynamics.dyn_covar

        # Set initial covariance (initial mu is always zero).
        sigma[0, idx_x, idx_x] = traj_info.x0sigma
        mu[0, idx_x] = traj_info.x0mu

        for t in range(T):
            sigma[t, :, :] = np.vstack([
                np.hstack([
                    sigma[t, idx_x, idx_x],
                    sigma[t, idx_x, idx_x].dot(traj_distr.K[t, :, :].T)
                ]),
                np.hstack([
                    traj_distr.K[t, :, :].dot(sigma[t, idx_x, idx_x]),
                    traj_distr.K[t, :, :].dot(sigma[t, idx_x, idx_x]).dot(
                        traj_distr.K[t, :, :].T
                    ) + traj_distr.pol_covar[t, :, :]
                ])
            ])
            mu[t, :] = np.hstack([
                mu[t, idx_x],
                traj_distr.K[t, :, :].dot(mu[t, idx_x]) + traj_distr.k[t, :] #K[t]x + k[t].
            ])
            if t < T - 1:
                sigma[t+1, idx_x, idx_x] = \
                        Fm[t, :, :].dot(sigma[t, :, :]).dot(Fm[t, :, :].T) + \
                        dyn_covar[t, :, :]
                mu[t+1, idx_x] = Fm[t, :, :].dot(mu[t, :]) + fv[t, :]
        return mu, sigma

    def backward(self, prev_traj_distr, traj_info, eta, algorithm, NN=True):
        """
        Perform LQR backward pass. This computes a new linear Gaussian
        policy object.
        Args:
            prev_traj_distr: A linear Gaussian policy object from
                previous iteration.
            traj_info: A TrajectoryInfo object.
            eta: Dual variable.
            algorithm: Algorithm object needed to compute costs.
            m: Condition number.
        Returns:
            traj_distr: A new linear Gaussian policy.
            new_eta: The updated dual variable. Updates happen if the
                Q-function is not PD.
        """
        # Constants.
        T = algorithm.T
        dU = algorithm.dU
        dX = algorithm.dX
        gamma = algorithm.gamma #discount factor

        traj_distr = prev_traj_distr.nans_like() # a new linear gaussian policy object, containing nans

        idx_x = slice(dX)
        idx_u = slice(dX, dX+dU)

        # Pull out dynamics (learned ) linearized at the beginning of the iteration
        Fm = traj_info.dynamics.Fm
        fv = traj_info.dynamics.fv

        # Non-SPD correction terms.
        del_ = 1e-4 #self._hyperparams['del0']
        eta0 = eta

        # Run dynamic programming.
        fail = True
        while fail:
            fail = False  # Flip to true on non-symmetric PD.

            # Allocate.
            Vxx = np.zeros((T, dX, dX))
            Vx = np.zeros((T, dX))
            Qtt = np.zeros((T, dX+dU, dX+dU))
            Qt = np.zeros((T, dX+dU))

            fCm, fcv = algorithm.compute_costs(eta, augment=True, NN=NN)

            # Compute state-action-state function at each time step.
            for t in range(T - 1, -1, -1):
                # Add in the cost.
                Qtt[t] = fCm[t, :, :]  # (X+U) x (X+U)
                Qt[t] = fcv[t, :]  # (X+U) x 1

                # Add in the value function from the next time step.
                if t < T - 1:
                    Qtt[t] += gamma*(Fm[t, :, :].T.dot(Vxx[t+1, :, :]).dot(Fm[t, :, :]))
                    Qt[t] += gamma*(Fm[t, :, :].T.dot(Vx[t+1, :]+Vxx[t+1, :, :].dot(fv[t, :])))

                # Symmetrize quadratic component.
                Qtt[t] = 0.5 * (Qtt[t] + Qtt[t].T)
                inv_term = Qtt[t, idx_u, idx_u]
                k_term = Qt[t, idx_u]
                K_term = Qtt[t, idx_u, idx_x]
            
                # Compute Cholesky decomposition of Q function action
                # component.
                try:
                    U = sp.linalg.cholesky(inv_term)
                    L = U.T
                except LinAlgError as e:
                    # Error thrown when Qtt[idx_u, idx_u] is not
                    # symmetric positive definite.
                    #LOGGER.debug('LinAlgError: %s', e)
                    fail = True
                    break  #break, and increase eta. 

                # Store conditional covariance, inverse, and Cholesky.
                traj_distr.inv_pol_covar[t, :, :] = inv_term
                traj_distr.pol_covar[t, :, :] = sp.linalg.solve_triangular(
                    U, sp.linalg.solve_triangular(L, np.eye(dU), lower=True)
                )
                traj_distr.chol_pol_covar[t, :, :] = sp.linalg.cholesky(
                    traj_distr.pol_covar[t, :, :]
                )  #upper trianglular in default.

                # Compute mean terms.
                traj_distr.k[t, :] = -sp.linalg.solve_triangular(
                    U, sp.linalg.solve_triangular(L, k_term, lower=True)
                )
                traj_distr.K[t, :, :] = -sp.linalg.solve_triangular(
                    U, sp.linalg.solve_triangular(L, K_term, lower=True)
                )
                
                # Compute value function.
                Vxx[t, :, :] = Qtt[t, idx_x, idx_x] + Qtt[t, idx_x, idx_u].dot(traj_distr.K[t, :, :])
                Vx[t, :] = Qt[t, idx_x] + Qtt[t, idx_x, idx_u].dot(traj_distr.k[t, :])
                Vxx[t, :, :] = 0.5 * (Vxx[t, :, :] + Vxx[t, :, :].T)

            # Increment eta on non-SPD Q-function.
            if fail:
                old_eta = eta
                eta = eta0 + del_
                #LOGGER.debug('Increasing eta: %f -> %f', old_eta, eta)
                del_ *= 2  # Increase del_ exponentially on failure.
                
                fail_check = (eta >= 1e16)
                if fail_check:
                    if np.any(np.isnan(Fm)) or np.any(np.isnan(fv)):
                        raise ValueError('NaNs encountered in dynamics!')
                    raise ValueError('Failed to find PD solution even for very \
                            large eta (check that dynamics and cost are \
                            reasonably well conditioned)!')
        return traj_distr, eta

    def _conv_check(self, con, kl_step):
        """Function that checks whether dual gradient descent has converged."""
        return abs(con) < 0.1 * kl_step

    def backward_wth_policy(self, traj_distr, traj_info, algorithm):
        #compute the quadratic approximation of cost-to-go of the given policy in traj_distr. 
        #traj_distr: the latest linear gaussian policies 
        #traj_info: contains the learned linear dynamics 

        T = traj_distr.T
        dU = traj_distr.dU
        dX = traj_distr.dX
        gamma = algorithm.gamma #discount factor

        idx_x = slice(dX)
        idx_u = slice(dX, dX+dU)

        # Pull out dynamics.
        Fm = traj_info.dynamics.Fm
        fv = traj_info.dynamics.fv
        Vxx = np.zeros((T, dX, dX))
        Vx = np.zeros((T, dX))
        Qtt = np.zeros((T, dX+dU, dX+dU))
        Qt = np.zeros((T, dX+dU))
        fCm, fcv = algorithm.compute_costs(algorithm.cur.eta, augment=False) #just use the cost. 
        for t in range(T - 1, -1, -1):
            # Add in the cost.
            Qtt[t] = fCm[t, :, :]  # (X+U) x (X+U)
            Qt[t] = fcv[t, :]  # (X+U) x 1
            if t < T - 1:
                Qtt[t] += gamma*(Fm[t, :, :].T.dot(Vxx[t+1, :, :]).dot(Fm[t, :, :]))
                Qt[t] += gamma*(Fm[t, :, :].T.dot(Vx[t+1, :]+Vxx[t+1, :, :].dot(fv[t, :])))
            
            Qtt[t] = 0.5 * (Qtt[t] + Qtt[t].T)
            #directly use the policy to compute V^{\pi}[t]:
            Vxx[t, :, :] = Qtt[t, idx_x, idx_x] + Qtt[t, idx_x, idx_u].dot(traj_distr.K[t, :, :])
            Vx[t, :] = Qt[t, idx_x] + Qtt[t, idx_x, idx_u].dot(traj_distr.k[t, :])
            Vxx[t, :, :] = 0.5 * (Vxx[t, :, :] + Vxx[t, :, :].T)
        
        return Qtt, Qt, Vxx, Vx