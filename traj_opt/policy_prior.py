""" This file defines the constant prior for policy linearization. """
import copy
from IPython import embed
import numpy as np
from algorithm_utils import gauss_fit_joint_prior

class PolicyPrior(object):
    """ Constant policy prior. """
    def __init__(self):
        self.strength = 1e-4

    def update(self, samples, policy_opt, all_samples, retrain=True):
        """ Update dynamics prior. """
        # Nothing to update for constant policy prior.
        pass

    def eval(self, Ts, Ps):
        """ Evaluate the policy prior. """
        dX, dU = Ts.shape[-1], Ps.shape[-1]
        prior_fd = np.zeros((dU, dX))
        prior_cond = 1e-5 * np.eye(dU)
        sig = np.eye(dX)
        Phi = self.strength * np.vstack([
            np.hstack([sig, sig.dot(prior_fd.T)]),
            np.hstack([prior_fd.dot(sig),
                       prior_fd.dot(sig).dot(prior_fd.T) + prior_cond])
        ])
        return np.zeros(dX+dU), Phi, 0, self.strength

    def fit_old_from_GPS(self, X, pol_mu, pol_sig):
        """
        Fit policy linearization.

        Args:
            X: Samples (N, T, dX)
            pol_mu: Policy means (N, T, dU)
            pol_sig: Policy covariance (N, T, dU)
        """
        assert X.ndim == 3
        N, T, dX = X.shape
        dU = pol_mu.shape[2]
        if N == 1:
            raise ValueError("Cannot fit dynamics on 1 sample")

        # Collapse policy covariances. (This is only correct because
        # the policy doesn't depend on state).
        pol_sig = np.mean(pol_sig, axis=0)

        # Allocate.
        pol_K = np.zeros([T, dU, dX])
        pol_k = np.zeros([T, dU])
        pol_S = np.zeros([T, dU, dU])

        # Fit policy linearization with least squares regression.
        dwts = (1.0 / N) * np.ones(N)
        for t in range(T):
            Ts = X[:, t, :]
            Ps = pol_mu[:, t, :]
            Ys = np.concatenate([Ts, Ps], axis=1)
            # Obtain Normal-inverse-Wishart prior.
            mu0, Phi, mm, n0 = self.eval(Ts, Ps)
            sig_reg = np.zeros((dX+dU, dX+dU))
            # Slightly regularize on first timestep.
            if t == 0:
                sig_reg[:dX, :dX] = 1e-8
            pol_K[t, :, :], pol_k[t, :], pol_S[t, :, :] = \
                    gauss_fit_joint_prior(Ys,
                            mu0, Phi, mm, n0, dwts, dX, dU, sig_reg)
        
        pol_S += pol_sig
        return pol_K, pol_k, pol_S

    def fit(self, X, pol_mu, pol_sig):
        assert X.ndim == 3
        N, T, dX = X.shape
        dU = pol_mu.shape[2]
        if N == 1:
            raise ValueError("Cannot fit dynamics on 1 sample")

        pol_sig = np.mean(pol_sig, axis=0)
        # Allocate.
        pol_K = np.zeros([T, dU, dX])
        pol_k = np.zeros([T, dU])
        pol_S = np.zeros([T, dU, dU])

        it = slice(dX)
        ip = slice(dX, dX+dU)
        for t in xrange(T):
            Ts = X[:,t,:]
            Ps = pol_mu[:,t,:]
            TP = np.c_[Ts,Ps]
            TP_mean = np.mean(TP,axis=0)
            empsig = (TP - TP_mean).T.dot(TP-TP_mean)/N
            sigma = 0.5*(empsig + empsig.T)
            sigma[it,it] += 1e-8*np.eye(sigma[it,it].shape[0])
            
            K = np.linalg.solve(sigma[it,it], sigma[it,ip]).T
            k = TP_mean[ip] - K.dot(TP_mean[it])

            pol_K[t,:,:] = K
            pol_k[t,:] = k

            covar = sigma[ip,ip] - K.dot(sigma[it,it]).dot(K.T)
            pol_S[t,:,:] = 0.5*(covar+covar.T) + np.diag(pol_sig[t])

        return pol_K,pol_k, pol_S

    def fit_alt_jacobian(self, X, pol_mu, pol_sig, jac):
        assert X.ndim == 3
        N, T, dX = X.shape
        dU = pol_mu.shape[2]
        #if N == 1:
        #    raise ValueError("Cannot fit dynamics on 1 sample")

        pol_sig = np.mean(pol_sig, axis=0)
        # Allocate.
        pol_K = np.zeros([T, dU, dX])
        pol_k = np.zeros([T, dU])
        pol_S = np.zeros([T, dU, dU])

        for t in xrange(T):
            X_t = X[:,t,:]
            U_t = pol_mu[:,t,:]
            x_t_mean = np.mean(X_t,axis=0)
            u_t_mean = np.mean(U_t,axis=0)
            pol_K[t] = jac(x_t_mean)
            pol_k[t] = u_t_mean - pol_K[t].dot(x_t_mean)
            pol_S[t] = pol_sig[t]
           
        return pol_K, pol_k, pol_S

            
            
