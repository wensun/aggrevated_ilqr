import numpy as np
import dynamics
from dynamics_lr import DynamicsLR
from policy_prior import PolicyPrior

#test dynamics:

np.random.seed(100)
A = np.random.randn(5,5)/10
B = np.random.randn(5,3)/10
K = np.random.randn(3,5)
k = np.random.randn(3)

tmp_Cov = np.random.randn(5,5)*0.001
Cov = tmp_Cov.T.dot(tmp_Cov)
tmp_PCov = np.random.randn(3)*0.001
PCov = np.diag(tmp_PCov*tmp_PCov)

def policy(x):
    return (np.sin(np.exp(K.dot(x) + k) + 10.))**2

def policy_jac(x):
    return np.exp(K.dot(x) + k).dot(K)


def generate_trajectory(A, B, Cov, T, N, U=None):
    X = np.zeros((N, T, A.shape[0]))
    if U is None:
        U = np.random.randn(N, T, B.shape[1])

    for n in xrange(N):
        X[n,0,:] = np.random.randn(A.shape[0])
        for i in xrange(T-1):
            X[n,i+1,:] = A.dot(X[n,i,:]) + B.dot(U[n,i,:])
            X[n,i+1,:] += np.random.multivariate_normal(np.zeros(A.shape[0]), cov = Cov)

    return X,U

def generate_trajecotry_action(A,B,Cov, K,k,PCov, T, N):
    X = np.zeros((N, T, A.shape[0]))
    U_m = np.zeros((N,T,B.shape[1]))
    U_sig= np.array([np.array([PCov for i in xrange(T)]) for j in xrange(N)])

    for n in xrange(N):
        #print n
        X[n,0,:] = np.random.rand(A.shape[0])*0.001
        U_m[n,0,:] = np.exp(K.dot(X[n,0,:]) + k)
        for i in xrange(T-1):
            U_real = U_m[n,i,:] + np.random.multivariate_normal(np.zeros(B.shape[1]),cov = PCov)
            X[n,i+1,:] = A.dot(X[n,i,:]) + B.dot(U_real)
            X[n,i+1,:] += np.random.multivariate_normal(np.zeros(A.shape[0]), cov= Cov)
            U_m[n,i+1,:] = policy(X[n,i+1,:])  # np.exp(K.dot(X[n,i+1,:]) + k)

    return X, U_m, U_sig


def test_policy_fit(X, pol_K, pol_k):
    total_error = 0.0
    real_u_norm = 0.0
    N = X.shape[0]
    T = X.shape[1]
    dX = X.shape[2]
    dU = U_m.shape[2]

    for n in xrange(N):
        for t in xrange(T):
            u_p = pol_K[t].dot(X[n, t,:]) + pol_k[t]
            u_r = policy(X[n,t,:])
            total_error += np.linalg.norm(u_p - u_r)
            real_u_norm += np.linalg.norm(u_r)
    return total_error / (T*N), real_u_norm/(T*N)



#learn dynamics:
#DymLR = DynamicsLR()
#X,U = generate_trajectory(A,B,Cov, 100, 26)
#DymLR.fit(X,U)

#test policy:
pol_info = PolicyPrior()
X, U_m, U_sig = generate_trajecotry_action(A,B,Cov, K,k,PCov, 100, 16)
print ("fiting policies...")
pol_K, pol_k, pol_S = pol_info.fit(X,U_m, U_sig)
#pol_K, pol_k, pol_S = pol_info.fit_alt_jacobian(X, U_m, U_sig, policy_jac)
error, u_norm = test_policy_fit(X+np.random.rand(X.shape[0],X.shape[1],X.shape[2])*0.01, 
        pol_K, pol_k)
print error, u_norm





