import numpy as np
from traj_opt import algorithm
from dynamics.cartpole_gym import CartPoleEnv
from dynamics.inverted_double_pendulum import Customized_InvertedDoublePendulumEnv
from dynamics.reacher import Customized_ReacherEnv
from dynamics.pusher import Customized_PusherEnv
from dynamics.helicopter import Helicopter_Hover, Helicopter_Funnel
from traj_opt.algorithm import Algorithm
from IPython import embed
from dynamics.inverted_double_pendulum import Customized_InvertedDoublePendulumEnv
from dynamics.Swimmer_rlpy import Swimmer
from dynamics.swimmer import Customized_SwimmerEnv

def roll_in(env, parameters, alg):
    T = parameters["T"]
    dX = parameters["dx"]
    dU = parameters["du"]
    N = parameters["timesteps_per_batch"] / T
    X = np.zeros((N, T, dX))
    U = np.zeros((N, T, dU))
    C = np.zeros((N, T))

    K = alg.cur.traj_distr.K
    k = alg.cur.traj_distr.k
    Cov = alg.cur.traj_distr.pol_covar
    for n in xrange(N):
        x = env.reset()
        for t in xrange(T):
            X[n,t,:] = x
            u = K[t].dot(x) + k[t] \
                + np.random.multivariate_normal(np.zeros(dU),Cov[t])
            U[n,t,:] = u
            x, c, done, _ = env.step(u)
            C[n,t] = c
    
    return X,U,C


def test_learned_cost(alg, X, U):
    error = 0.
    N = X.shape[0]
    T = X.shape[1]
    dX = X.shape[2]
    dU = U.shape[2]
    Cm = alg.cur.traj_info.Cm
    cv = alg.cur.traj_info.cv
    cc = alg.cur.traj_info.cc
    cs = alg.cur.cs
    for n in xrange(N):
        for t in xrange(T):
            x_a = np.r_[X[n,t,:],U[n,t,:]]
            p_c = 0.5*x_a.dot(Cm[t]).dot(x_a)\
                + x_a.dot(cv[t]) + cc[t]
            r_c = cs[n,t]

            error += (p_c - r_c)**2
    return error/(N*T)


def test_LQG_kl(env, alg, parameters):
    #roll-in:
    for it in xrange(200):
        X,U,C = roll_in(env, parameters, alg)
        print X[0][:, 5]
        alg.memory_X.append(X)
        alg.memory_U.append(U)
        if len(alg.memory_X) > alg.history_k:
            alg.memory_X.pop(0) #pop the old ones. 
            alg.memory_U.pop(0) 

        avg_c = np.mean(np.sum(C,axis=1))

        print "at iteration {}, the current average cost is {}".format(it, avg_c)
        alg._update_dynamics(np.concatenate(alg.memory_X,axis=0),
                np.concatenate(alg.memory_U,axis=0)) #fit dynamics
        alg._eval_cost(X,U,C) #quadratize cost functions
        new_traj_distr, eta = alg.traj_opt.update(alg,NN=False) #dual update
        alg.cur.traj_distr = new_traj_distr #update linear-gauss
        alg.cur.eta = eta


np.random.seed(100)
#env = CartPoleEnv()
#env = Customized_InvertedDoublePendulumEnv()
#env = Customized_ReacherEnv()
#env = Customized_PusherEnv()
#env = Helicopter_Hover()
#env = Helicopter_Funnel()
env = Customized_SwimmerEnv()
#env = Customized_InvertedDoublePendulumEnv()
#env = Swimmer()
#test iterative LQG-ish: 
#the goal is to test if the forward and backward process in GPS are correct.
parameters = {"T":100, "du":env.action_space.shape[0], "dx":env.observation_space.shape[0], 
              "kl_step":0.5,
        "initial_state_var":0.01, "gamma":0.995, "timesteps_per_batch":20000,
        "history_k":2, "learn_cost":0}

alg = Algorithm(env, env.observation_space, env.action_space, parameters)
test_LQG_kl(env, alg, parameters)













