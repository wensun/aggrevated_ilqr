import numpy as np
from dynamics.reacher import Customized_ReacherEnv
from traj_opt.algorithm import Algorithm
from dynamics.pusher import Customized_PusherEnv

np.random.seed(100)
T = 100
dX = 20 
dU = 7
env = Customized_PusherEnv()
#env = Customized_ReacherEnv()
parameters = {'kl_step':0.1, 'initial_state_var':0.1}

alg = Algorithm(env, None, T, 2, 11, None, parameters)
X = np.random.randn(5, T, dX)
U = np.random.randn(5, T, dU)

alg._eval_cost(X,U)

#evaluate:
def evaluate_approximated_costs(X, U, Cm, cv, cc, cs):
    error = 0.
    T = Cm.shape[0]
    N = X.shape[0]
    print T, N
    for n in xrange(N):
        for t in xrange(T):
            x = X[n,t,:]
            u = U[n,t,:]
            xu = np.r_[x,u]
            value_p = 0.5*xu.dot(Cm[t]).dot(xu) + xu.dot(cv[t]) + cc[t]
            true_p = env.cost(x, u)
            error += np.abs(value_p - true_p)
    
    return error / (T*N)

err = evaluate_approximated_costs(X,U, alg.cur.traj_info.Cm,
        alg.cur.traj_info.cv, alg.cur.traj_info.cc, alg.cur.cs)
print err

