import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env

class Customized_StrikerEnv(mujoco_env.MujocoEnv, utils.EzPickle):
    def __init__(self):
        utils.EzPickle.__init__(self)
        self._striked = False
        self._min_strike_dist = np.inf
        #self._min_strike_vec = np.array([np.inf,np.inf,np.inf])
        self._min_strike_vec = np.zeros(3)
        self.strike_threshold = 0.1
        mujoco_env.MujocoEnv.__init__(self, 'striker.xml', 5)

    def _step(self, a):
        vec_1 = self.get_body_com("object") - self.get_body_com("tips_arm")
        vec_2 = self.get_body_com("object") - self.get_body_com("goal")
        #self._min_strike_dist = min(self._min_strike_dist, np.linalg.norm(vec_2))
        if np.linalg.norm(vec_2) <= self._min_strike_dist:
            self._min_strike_dist = np.linalg.norm(vec_2)
            self._min_strike_vec = vec_2

        if np.linalg.norm(vec_1) < self.strike_threshold:
            self._striked = True
            self._strike_pos = self.get_body_com("tips_arm")

        if self._striked:
            vec_3 = self.get_body_com("object") - self._strike_pos
            reward_near = -vec_3.dot(vec_3)#- np.linalg.norm(vec_3)
        else:
            reward_near = -vec_1.dot(vec_1)#- np.linalg.norm(vec_1)

        reward_dist = -self._min_strike_vec.dot(self._min_strike_vec)#- np.linalg.norm(self._min_strike_dist)
        reward_ctrl = - np.square(a).sum()
        #reward = 3 * reward_dist + 0.1 * reward_ctrl + 0.5 * reward_near
        reward = 0.5*16.* reward_dist + 0.1 * reward_ctrl + 0.5 * reward_near

        self.do_simulation(a, self.frame_skip)
        ob = self._get_obs()
        done = False

        return ob, -reward, done, dict(
                reward_dist=reward_dist,
                reward_ctrl=reward_ctrl,
                hit = self._striked, 
                reach_goal = (np.linalg.norm(self._min_strike_vec)<0.08)) #0.08 is the radius of the goal region.

    def viewer_setup(self):
        self.viewer.cam.trackbodyid = 0
        self.viewer.cam.distance = 4.0

    def reset_model(self):
        self._min_strike_dist = np.inf
        #self._min_strike_vec = np.array([np.inf,np.inf,np.inf])
        self._min_strike_vec = np.zeros(3)
        self._striked = False
        self._strike_pos = None

        qpos = self.init_qpos

        self.ball = np.array([0.5, -0.175])
        while True:
            self.goal = np.concatenate([
                    #self.np_random.uniform(low=0.15, high=0.7, size=1),
                    #self.np_random.uniform(low=0.1, high=1.0, size=1)])
                    self.np_random.uniform(low=0.15, high=0.25, size=1),
                    self.np_random.uniform(low=0.1, high=0.2, size=1)])
            if np.linalg.norm(self.ball - self.goal) > 0.17:
                break

        qpos[-9:-7] = [self.ball[1], self.ball[0]]
        qpos[-7:-5] = self.goal
        diff = self.ball - self.goal
        angle = -np.arctan(diff[0] / (diff[1] + 1e-8))
        qpos[-1] = angle / 3.14
        qvel = self.init_qvel + self.np_random.uniform(low=-.1, high=.1,
                size=self.model.nv)
        qvel[7:] = 0
        self.set_state(qpos, qvel)
        return self._get_obs()

    '''
    def _get_obs(self):
        return np.concatenate([
            self.model.data.qpos.flat[:7],
            self.model.data.qvel.flat[:7],
            self.get_body_com("tips_arm"),
            self.get_body_com("object"),
            self.get_body_com("goal"),
        ])
    '''
    def _get_obs(self):
        if self._striked is True: #alreay striked:
            return np.concatenate([
                self.model.data.qpos.flat[:7],
                self.model.data.qvel.flat[:7],
                self.get_body_com("object")-self._strike_pos,
                self._min_strike_vec,
                #self.get_body_com("object")-self.get_body_com("goal"),
            ])
        else:  #haven't striked
            return np.concatenate([
                self.model.data.qpos.flat[:7],
                self.model.data.qvel.flat[:7],
                self.get_body_com("object")-self.get_body_com("tips_arm"),
                self._min_strike_vec,
                #self.get_body_com("object")-self.get_body_com("goal"),
            ])

    def cost(self, obs, a):
        vec_1 = obs[-6:-3]
        vec_2 = obs[-3:]
        #return 3*vec_2.dot(vec_2) + 0.5*vec_1.dot(vec_1) + 0.1*a.dot(a)
        return 0.5*16*vec_2.dot(vec_2) + 0.5*vec_1.dot(vec_1) + 0.1*a.dot(a)

    def eval(self, X, U, C = None):
        tmpA = np.zeros((6, X.shape[1]))
        tmpA[0:3, -6:-3] = np.eye(3)
        tmpA[3:, -3:] = np.eye(3)*4.
        T,dX = X.shape
        dU = U.shape[1]
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i],U[i])

        lxx = np.array([tmpA.T.dot(tmpA)]*T) + 0.*np.eye(X.shape[1])
        luu = np.array([0.2*np.eye(dU)]*T)
        lux = np.array([np.zeros((dU,dX))]*T)
        lx = X.dot(tmpA.T.dot(tmpA))
        lu = 0.2*U   

        return l, lx, lu, lxx, luu, lux  















