import numpy as np
import math
from IPython import embed
from gym.spaces import Box
import scipy.io as sio

class Helicopter_Hover(object):

    def __init__(self):
        self.dt = 0.05
        self.g = 9.81

        self.idx = {}
        k = 0
        self.idx.update({"u_prev":range(k,k+4)})
        k += 4
        self.idx.update({"u_delta_prev":range(k,k+4)})
        k += 4
        self.idx.update({"ned_dot":range(k,k+3)})
        k += 3
        self.idx.update({"ned":range(k,k+3)})
        k += 3
        self.idx.update({"pqr":range(k,k+3)})
        k += 3
        self.idx.update({"q":range(k,k+4)})
        k += 4
        self.idx.update({"one":k})
        k += 1
        self.idx.update({"inputs":range(k,k+4)})
        k += 4
        self.idx.update({"uvw":range(k, k+3)})
        k += 3

        self.params = {}
        self.params.update({"m":5})
        self.params.update({"Ixx":0.3})
        self.params.update({"Iyy":0.3})
        self.params.update({"Izz":0.3})
        self.params.update({"Ixy":0})
        self.params.update({"Ixz":0})
        self.params.update({"Iyz":0})
        I = np.zeros((3,3))
        I[0] = np.array([self.params["Ixx"],self.params["Ixy"],self.params["Ixz"]])
        I[1] = np.array([self.params["Ixy"],self.params["Iyy"],self.params["Iyz"]])
        I[2] = np.array([self.params["Ixz"],self.params["Iyz"],self.params["Izz"]])
        self.params.update({"I":I})

        Tx = np.array([-.086, -3.47, 13.20])*self.params["Ixx"]
        Ty = np.array([.015, -3.06, -9.21])*self.params["Iyy"]
        Tz = np.array([-.139, -2.58, 14.84])*self.params["Izz"]
        Fx = -.048 * self.params["m"]
        Fy = np.array([-0.57,-.12])*self.params["m"]
        Fz = np.array([1.21,-.0005,-27.5])*self.params["m"]

        self.params.update({"Tx":Tx, "Ty":Ty, "Tz":Tz, 
                "Fx":Fx, "Fy":Fy, "Fz":Fz})

        self.features = {}
        Tx_f = [self.idx["one"], self.idx["pqr"][0], self.idx["inputs"][0]]
        Ty_f = [self.idx["one"],self.idx["pqr"][1], self.idx["inputs"][1]]
        Tz_f = [self.idx["one"],self.idx["pqr"][2],self.idx["inputs"][2]]
        Fx_f = [self.idx["uvw"][0]]
        Fy_f = [self.idx["one"], self.idx["uvw"][1]]
        Fz_f = [self.idx["one"], self.idx["uvw"][2], self.idx["inputs"][3]]
        self.features.update({"Tx":Tx_f, "Ty":Ty_f, "Tz":Tz_f,
            "Fx":Fx_f, "Fy":Fy_f, "Fz":Fz_f})


        self.state = None
        self.t = 0

        self.aileron_trim = -self.params["Tx"][0]/self.params["Tx"][2]
        self.elevator_trim= -self.params["Ty"][0]/self.params["Ty"][2]
        self.rudder_trim = -self.params["Tz"][0]/self.params["Tz"][2]

        self.roll_angle_trim = -math.asin(self.params["Fy"][0]/(self.params["m"]*self.g))
        self.collective_trim = (self.params["m"]*(-self.g)*math.cos(self.roll_angle_trim)
            -self.params["Fz"][0])/self.params["Fz"][2]

        self.control_trims = np.array([self.aileron_trim,self.elevator_trim,
            self.rudder_trim, self.collective_trim])
        self.quaternion_trim = np.array([math.sin(self.roll_angle_trim/2.), 
            0, 0, math.cos(self.roll_angle_trim/2.)])
        self.target_hover_state = np.r_[self.control_trims, np.zeros(4), 
            np.zeros(3), np.zeros(3), np.zeros(3), self.quaternion_trim]
        self.state_multipliers = np.r_[
            0*np.ones(4), 1000*np.ones(4),1.*np.ones(3), np.ones(3),
            np.ones(3), np.ones(3), 0]

        self.Q = np.diag(self.state_multipliers[0:-1])
        self.R = np.zeros((4,4))

        self.action_space = Box(low=-np.inf, 
                high=np.inf, shape=(4,))
        self.observation_space = Box(low =-np.inf, high=np.inf, shape=(self.target_hover_state.shape[0]-1,))

        self.magic_factor = 0.95

    def cost(self, obs, a): #note obs is the relative positive to the target hover pos
        return obs.dot(self.Q).dot(obs)

    def eval(self, X, U, C = None):
        assert X.ndim == 2 and U.ndim == 2
        dX = X.shape[1]
        dU = U.shape[1]
        T = X.shape[0]
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])
        
        lx = 2*X.dot(self.Q)
        lu = 2*U.dot(self.R)
        luu = np.array([2*self.R]*T)
        lxx = np.array([2*self.Q]*T)
        lux = np.array([np.zeros((dU,dX))]*T)

        return l, lx, lu, lxx, luu, lux


    def _quat_multiply(self, lq, rq):
        quat = np.zeros(4);
        quat[0] = lq[3]*rq[0] + lq[0]*rq[3] + lq[1]*rq[2] - lq[2]*rq[1];
        quat[1] = lq[3]*rq[1] - lq[0]*rq[2] + lq[1]*rq[3] + lq[2]*rq[0];
        quat[2] = lq[3]*rq[2] + lq[0]*rq[1] - lq[1]*rq[0] + lq[2]*rq[3];
        quat[3] = lq[3]*rq[3] - lq[0]*rq[0] - lq[1]*rq[1] - lq[2]*rq[2];
        return quat

    def _quaternion_from_axis_rotation(self, axis_rotation):
        rotation_angle = np.linalg.norm(axis_rotation);
        quat = np.zeros(4);
        if rotation_angle < 1e-4:  # avoid division by zero -- also: can use simpler computation in this case, since for small angles sin(x) = x is a good approximation
	        quat[0:3] = axis_rotation/2.;
        else:
            normalized_axis = axis_rotation / rotation_angle;
            quat[0:3] = normalized_axis * math.sin(rotation_angle/2.);
        quat[-1] = np.sqrt(1. - quat[0:3].dot(quat[0:3]))
        return quat

    def _rotate_vector(self, vin, q):
        vout = self._quat_multiply(self._quat_multiply(q, np.r_[vin,0]), 
            np.r_[-q[0:3], q[-1]])
        vout = vout[0:3]
        return vout

    def compute_dx(self, xtarget, x):
        dx = x - xtarget
        #dx[0:4] = 0
        dx[-4:] = self._quat_multiply(
            np.array([-1,-1,-1,1])*xtarget[-4:],
            x[-4:])
        dx[-1] = 1.
        return dx[0:-1]

    def _express_vector_in_quat_frame(self, vin, q):
        vout = self._rotate_vector(vin, np.r_[-q[0:3],q[-1]])
        return vout

    def step(self, a):
        x0 = np.copy(self.state)
        step_cost = self.cost(self.compute_dx(self.target_hover_state,x0), a)

        x1 = np.zeros(x0.shape[0])

        x1[self.idx["u_prev"]] = x0[self.idx["u_prev"]] + a
        x1[self.idx["u_delta_prev"]] = a
        
        x1[self.idx["ned"]] = x0[self.idx["ned"]]+self.dt*x0[self.idx["ned_dot"]]
        x1[self.idx["q"]] = self._quat_multiply(x0[self.idx["q"]], 
            self._quaternion_from_axis_rotation(self.dt*x0[self.idx["pqr"]]))
        
        x0 = np.r_[x0, 1]
        x0 = np.r_[x0, x1[self.idx["u_prev"]]]
        x0 = np.r_[x0, self._express_vector_in_quat_frame(x0[self.idx["ned_dot"]],
               x0[self.idx["q"]])]
        assert x0.shape[0] == 29

        Fxyz_minus_g = np.zeros(3)
        Fxyz_minus_g[0] = x0[self.features["Fx"]].dot(self.params["Fx"])
        Fxyz_minus_g[1] = x0[self.features["Fy"]].dot(self.params["Fy"])
        Fxyz_minus_g[2] = x0[self.features["Fz"]].dot(self.params["Fz"])

        F_ned_minus_g = self._rotate_vector(Fxyz_minus_g, x0[self.idx["q"]])
        F_ned = F_ned_minus_g + self.params["m"] * np.array([0,0,9.81])

        model_bias = np.random.randn(6)*1e-5
        F_ned = F_ned + model_bias[0:3]

        x1[self.idx["ned_dot"]] = x0[self.idx["ned_dot"]] + self.dt*F_ned/self.params["m"]
        Tx = x0[self.features["Tx"]].dot(self.params["Tx"]) + model_bias[3]
        Ty = x0[self.features["Ty"]].dot(self.params["Ty"]) + model_bias[4]
        Tz = x0[self.features["Tz"]].dot(self.params["Tz"]) + model_bias[5]

        pqr_dot = np.array([Tx/self.params["Ixx"],Ty/self.params["Iyy"],
            Tz/self.params["Izz"]])
        
        x1[self.idx["pqr"]] = x0[self.idx["pqr"]] + self.dt*pqr_dot

        x1 = self.magic_factor * x1 + (1-self.magic_factor)*self.target_hover_state
        self.state = np.copy(x1)
        self.t = self.t + 1

        return self.compute_dx(self.target_hover_state, x1), step_cost, False, {} #directly return the relative position to hover target


    def reset(self):
        self.t = 0
        start_ned = np.array([1,0,0]) + np.random.randn(3) * 1e-5 #starting from 20m from the north of the hover position.
        rotation_axis = np.array([0,0,1])
        rotation_angle = 0 + np.random.randn()*1e-5
        start_q = np.c_[([math.sin(rotation_angle/2)*rotation_axis]), 
                    math.cos(rotation_angle/2)]
        start_state = self.target_hover_state
        start_state[self.idx["ned"]] = start_ned
        start_state[self.idx["q"]] = start_q
        self.state = start_state
        self.state = self.magic_factor*self.state + (1.-self.magic_factor)*self.target_hover_state
        return self.compute_dx(self.target_hover_state, self.state) #directly assume we observe the difference 

    def seed(self, seed):
        pass


class Helicopter_Funnel(Helicopter_Hover):
    def __init__(self):
        Helicopter_Hover.__init__(self)
        self.timestep_limit = 600
        
        hover_period = 5
        hover_numtics = 5/self.dt
        num_funnels = 4
        f_radius = 5
        f_period = 5
        f_numtics = 5/self.dt
        f_start = hover_numtics+1

        self.magic_factor = 0.85

        hover_quaternion_target = np.array([0,0,0,1])
        hover_ned_target = np.array([-f_radius,0,0])
        target_hover_state = np.r_[np.zeros(4), np.zeros(4), np.zeros(3), 
                np.zeros(3), np.zeros(3),hover_quaternion_target]
        target_hover_state[self.idx["ned"]] = hover_ned_target

        self.target_traj_X = sio.loadmat("dynamics/target_x.mat")["X"]
        assert (600,21) == self.target_traj_X.shape

        #self.state_multipliers = np.r_[
        #    0*np.ones(4), 1000*np.ones(4), 0.01*np.ones(3), np.ones(3),
        #    np.ones(3), np.ones(3), 0]
        
        self.state_multipliers = np.r_[
            0*np.ones(4), 1000*np.ones(4), 0.01*np.ones(3), np.ones(3),
            np.ones(3), np.ones(3), 0]

        self.Q = np.diag(self.state_multipliers[0:-1])
        self.R = np.eye(4)*0.
        
        self.state = None
        self.t = 0

    def cost(self, obs, a): #obs is the relative position to target_t
        return obs.dot(self.Q).dot(obs)

    def eval(self, X, U, C = None): #X is the relative position to targets 
        assert X.ndim == 2 and U.ndim == 2
        dX = X.shape[1]
        dU = U.shape[1]
        T = X.shape[0]
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])
        
        lx = 2*X.dot(self.Q)
        lu = U.dot(self.R)
        luu = np.array([self.R]*T)
        lxx = np.array([2*self.Q]*T)+1e-8*np.eye(self.Q.shape[0])
        lux = np.array([np.zeros((dU,dX))]*T)

        return l, lx, lu, lxx, luu, lux
        
    
    def step(self, a):
        x0 = np.copy(self.state)
        curr_target = self.target_traj_X[self.t]
        #use the relative position to the current target to compute the cost. 
        step_cost = self.cost(self.compute_dx(curr_target,x0), a)

        x1 = np.zeros(x0.shape[0])

        x1[self.idx["u_prev"]] = x0[self.idx["u_prev"]] + a
        x1[self.idx["u_delta_prev"]] = a
        
        x1[self.idx["ned"]] = x0[self.idx["ned"]]+self.dt*x0[self.idx["ned_dot"]]
        x1[self.idx["q"]] = self._quat_multiply(x0[self.idx["q"]], 
            self._quaternion_from_axis_rotation(self.dt*x0[self.idx["pqr"]]))
        
        x0 = np.r_[x0, 1]
        x0 = np.r_[x0, x1[self.idx["u_prev"]]]
        x0 = np.r_[x0, self._express_vector_in_quat_frame(x0[self.idx["ned_dot"]],
               x0[self.idx["q"]])]
        assert x0.shape[0] == 29

        Fxyz_minus_g = np.zeros(3)
        Fxyz_minus_g[0] = x0[self.features["Fx"]].dot(self.params["Fx"])
        Fxyz_minus_g[1] = x0[self.features["Fy"]].dot(self.params["Fy"])
        Fxyz_minus_g[2] = x0[self.features["Fz"]].dot(self.params["Fz"])

        F_ned_minus_g = self._rotate_vector(Fxyz_minus_g, x0[self.idx["q"]])
        F_ned = F_ned_minus_g + self.params["m"] * np.array([0,0,9.81])

        model_bias = np.random.randn(6)*1e-3
        F_ned = F_ned + model_bias[0:3]

        x1[self.idx["ned_dot"]] = x0[self.idx["ned_dot"]] + self.dt*F_ned/self.params["m"]
        Tx = x0[self.features["Tx"]].dot(self.params["Tx"]) + model_bias[3]
        Ty = x0[self.features["Ty"]].dot(self.params["Ty"]) + model_bias[4]
        Tz = x0[self.features["Tz"]].dot(self.params["Tz"]) + model_bias[5]

        pqr_dot = np.array([Tx/self.params["Ixx"],Ty/self.params["Iyy"],
            Tz/self.params["Izz"]])
        
        x1[self.idx["pqr"]] = x0[self.idx["pqr"]] + self.dt*pqr_dot

        self.t = self.t + 1

        new_target = self.target_traj_X[self.t]
        x1 = self.magic_factor * x1 + (1-self.magic_factor)*new_target
        self.state = np.copy(x1)

        return self.compute_dx(new_target, x1), step_cost, False, {} #directly return the relative position to the next target


    def reset(self):
        self.t = 0
        start_ned = np.array([0,0,0]) + np.random.randn(3) * 1e-2
        rotation_axis = np.array([0,0,1])
        rotation_angle = 0 + np.random.randn()*1e-2
        start_q = np.c_[([math.sin(rotation_angle/2)*rotation_axis]), 
                    math.cos(rotation_angle/2)]
        start_state = self.target_hover_state
        start_state[self.idx["ned"]] = start_ned
        start_state[self.idx["q"]] = start_q
        self.state = self.target_traj_X[self.t]  #start_state
        self.state = self.magic_factor*self.state + (1-self.magic_factor)*self.target_traj_X[self.t]
        #compute relative position to the first target.
        return self.compute_dx(self.target_traj_X[0], self.state) #directly observe the difference 




#test
#env = Helicopter_Hover()
env = Helicopter_Funnel()
env.state = env.target_hover_state

#x, c, _, _ = env.step(np.ones(4))
