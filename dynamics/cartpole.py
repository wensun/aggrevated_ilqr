#dynamics examples: for test purpose. 

import numpy as np
import math
from LQG import Linear_Quadratic_Gaussian
from IPython import embed

class CartPole(object):
    def __init__(self):
        self.gravity = 9.8
        self.masscart = 1.0
        self.masspole = 0.1
        self.total_mass = (self.masspole + self.masscart)
        self.length = 0.5 # actually half the pole's length
        self.polemass_length = (self.masspole * self.length)
        self.force_mag = 10.0
        self.tau = 0.02  # seconds between state updates

        # Angle at which to fail the episode
        self.theta_threshold_radians = 12 * 2 * math.pi / 360
        self.x_threshold = 2.4

        self.goal =  np.zeros(4) #np.array([1.0, 0.0, self.theta_threshold_radians, 0.0])

        # Angle limit set to 2 * theta_threshold_radians so failing observation is still within bounds
        high = np.array([
            self.x_threshold * 2,
            np.finfo(np.float32).max,
            self.theta_threshold_radians * 2,
            np.finfo(np.float32).max])

        self.steps_beyond_done = None
        self.state = None


    def _step(self, action):
        state = self.state.copy
        x, x_dot, theta, theta_dot = state
        force = action[0]
        #print theta
        costheta = math.cos(theta)
        sintheta = math.sin(theta)
        temp = (force + self.polemass_length * theta_dot * theta_dot * sintheta) / self.total_mass
        thetaacc = (self.gravity * sintheta - costheta* temp) / (self.length * (4.0/3.0 - self.masspole * costheta * costheta / self.total_mass))
        xacc  = temp - self.polemass_length * thetaacc * costheta / self.total_mass
        x  = x + self.tau * x_dot
        x_dot = x_dot + self.tau * xacc
        theta = theta + self.tau * theta_dot
        
        if theta > 2*math.pi:
            theta = theta - np.floor(theta/2./math.pi)*2.*math.pi
        elif theta < -2*math.pi:
            theta = theta + np.floor(np.abs(theta)/2./math.pi)*2*math.pi
            
        theta_dot = theta_dot + self.tau * thetaacc
        next_state = (x,x_dot,theta,theta_dot)
        done =  x-self.goal[0] < -self.x_threshold \
                or x-self.goal[0] > self.x_threshold \
                or theta-self.goal[2] < -self.theta_threshold_radians \
                or theta-self.goal[2] > self.theta_threshold_radians
        done = bool(done)

        return np.array(next_state), 0, done, {}

    def f(self, x, a):
        return self._step(x, a)[0]

    def reward(self, state, action):
        S = np.eye(state.shape[0])
        S[0,0] = 100
        S[2,2] = 1
        S[3,3] = 100
        A = np.eye(action.shape[0])*0.01
        return -(0.5*(state-self.goal).dot(S).dot(state-self.goal) + 0.5*action.dot(A).dot(action))
        #return 0.5*state.dot(S).dot(state) + 0.5*action.dot(A).dot(action)

        #return 0.5*state.dot(np.eye(state.shape[0])).dot(state) + 0.5*action.dot(np.eye(action.shape[0])).dot(action)


def finite_difference(f, x, a, delta = 0.00123):
    #compute jacobians:
    x_next = f(x,a)
    x_next = np.array(x_next)
    if np.ndim(x_next) == 0:
        A = np.zeros((1, x.shape[0]))
        B = np.zeros((1, a.shape[0])) 
    else:
        A = np.zeros((x_next.shape[0], x.shape[0]))
        B = np.zeros((x_next.shape[0], a.shape[0]))
    #compute one dim by one dim:
    for i in xrange(x.shape[0]):
        x_p = x.copy()
        x_p[i] += delta
        x_next_p = f(x_p, a)
        x_m = x.copy()
        x_m[i] -= delta
        x_next_m = f(x_m, a)
        A[:,i] = (x_next_p - x_next_m)/(2.*delta)

    for i in xrange(a.shape[0]):
        a_p = a.copy()
        a_p[i] += delta
        x_next_p = f(x, a_p)
        a_m = a.copy()
        a_m[i] -= delta
        x_next_m = f(x, a_m)
        B[:,i] = (x_next_p - x_next_m)/(2.*delta)
    
    if np.ndim(x_next) == 0:
        return A[0],B[0]
    else:
        return A,B

def Hessian_finite_difference(f, x, a, delta = 0.00123):
    def new_f(xa):
        return f(xa[0:x.shape[0]], xa[x.shape[0]:])

    xa = np.concatenate([x,a],axis=0)
    H = np.zeros((xa.shape[0],xa.shape[0]))
    for i in range(xa.shape[0]):
        for j in range(i, xa.shape[0]):
            xa_pp = xa.copy()
            xa_pp[i] += delta
            xa_pp[j] += delta
            xa_mm = xa.copy()
            xa_mm[i] -= delta
            xa_mm[j] -= delta
            xa_pm = xa.copy()
            xa_pm[i] += delta
            xa_pm[j] -= delta
            xa_mp = xa.copy()
            xa_mp[i] -= delta
            xa_mp[j] += delta

            H[i,j] = ((new_f(xa_pp) - new_f(xa_mp)) - (new_f(xa_pm) - new_f(xa_mm)))/(4*delta**2)
            H[j,i] = H[i,j]
    
    return H[0:x.shape[0],0:x.shape[0]], H[x.shape[0]:xa.shape[0],x.shape[0]:xa.shape[0]], H[x.shape[0]:xa.shape[0], 0:x.shape[0]]
    #C_xx, C_uu, C_ux

'''
env = CartPole()
#env = RandomDynamics()
x_0 = env.goal #np.zeros(4) # np.random.randn(5)*1 #np.zeros(5)
a_0 = np.zeros(1) #np.random.randn(3)*1 #np.zeros(3)
A = np.zeros((4,4))
B = np.zeros((4,1))
A,B = finite_difference(env.f, x_0,a_0)
#A = env.A
#B = env.B
b = env.f(x_0,a_0) - A.dot(x_0) - B.dot(a_0)    #np.zeros(4)
F = np.eye(4)*0.001

x = np.random.randn(4)*0.001 + x_0
a = np.random.randn(1)*0.001 + a_0

C_xx, C_uu, C_ux = Hessian_finite_difference(env.reward, x_0,a_0)
#C_xx = env.C[0:5,0:5]
#C_uu = env.C[5:,5:]
#C_ux = env.C[5:,0:5]
C_x, C_u = finite_difference(env.reward, x_0, a_0)
C = env.reward(x_0,a_0) - C_x.dot(x_0) - C_u.dot(a_0) + 0.5*x_0.dot(C_xx).dot(x_0) + 0.5*a_0.dot(C_uu).dot(a_0) + a_0.dot(C_ux).dot(x_0)
C_x = C_x - C_ux.transpose().dot(a_0) - C_xx.dot(x_0)
C_u = C_u - C_ux.dot(x_0) - C_uu.dot(a_0)

#C_x = C_xp - C_xx.dot(x_0) - C_ux.transpose().dot(a_0)
#C_u = C_up - C_uu.dot(a_0) - C_ux.dot(x_0)
#C = (env.reward(x_0,a_0) - C_x.dot(x_0) - C_u.dot(a_0) + 0.5*x_0.dot(C_xx).dot(x_0) + 0.5*a_0.dot(C_uu).dot(a_0)
#        + a_0.dot(C_ux).dot(x_0))

real_r = env.reward(x,a)
test_r2 = C + 0.5*x.dot(C_xx).dot(x) + 0.5*a.dot(C_uu).dot(a) + a.dot(C_ux).dot(x) + C_x.dot(x) + C_u.dot(a)
#test_r = env.reward(x_0,a_0) + 0.5*(x-x_0).dot(C_xx).dot(x-x_0) + 0.5*(a-a_0).dot(C_uu).dot(a-a_0) + (a-a_0).dot(C_ux).dot(x-x_0)+C_xp.dot(x-x_0) + C_up.dot(a-a_0)


#d_r = real_r - test_r
#print d_r
#print real_r - test_r2

T = 200
As = [A]*(T-1)
Bs = [B]*(T-1)
bs = [b]*(T-1)
Fs = [F]*(T-1)
C_xs = [C_x]*T
C_us = [C_u]*T
C_xxs = [C_xx]*T
C_uus = [C_uu]*T
C_uxs = [C_ux]*T
Cs = [C]*T


gamma = 0.995
gammas = np.array([gamma**i for i in range(T)])
Ks, ks, Covs, V_xxs, V_xs, vs = Linear_Quadratic_Gaussian(As,Bs,bs, Fs,C_xs, C_us, C_xxs, C_uus, C_uxs, Cs, gamma = 0.9, reg = 1e-8)

#test:
traj_xs = []
traj_as = []
traj_rs = []
x = env.goal + np.random.rand(4)*0.001
for i in xrange(T):
    a = Ks[i].dot(x) + ks[i]
    r = env.reward(state=x, action=a)
    traj_xs.append(x)
    traj_as.append(a)
    traj_rs.append(r)

    x_p, _, done,_ = env._step(x, a)
    x = x_p
    if done:
        #print i
        break
#print i
vtg = np.array([np.sum(traj_rs[i:]*gammas[0:len(traj_rs[i:])]) for i in range(T)])
vtg_app = np.array([(0.5*traj_xs[i].dot(V_xxs[i]).dot(traj_xs[i])+V_xs[i].dot(traj_xs[i])+vs[i]) for i in range(T)])

dd = vtg - vtg_app

'''




