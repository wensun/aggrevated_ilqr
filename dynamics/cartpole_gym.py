"""
Classic cart-pole system implemented by Rich Sutton et al.
Copied from http://incompleteideas.net/sutton/book/code/pole.c
permalink: https://perma.cc/C9ZM-652R
"""

import math
import numpy as np
import gym
from gym import spaces
from gym.utils import seeding
from gym.spaces import Box
from IPython import embed


class CartPoleEnv(gym.Env):
    
    #modification from classic cartpole from gym:
    # 1. initialize from 0.05 to 0.01
    # 2. made it continuous 1-d control


    def __init__(self):
        #embed()
        self.gravity = 9.8
        self.masscart = 1.0
        self.masspole = 0.1
        self.total_mass = (self.masspole + self.masscart)
        self.length = 0.5 # actually half the pole's length
        self.polemass_length = (self.masspole * self.length)
        self.force_mag = 10.0
        self.tau = 0.02  # seconds between state updates

        self.action_space = spaces.Box(low=-self.force_mag, 
                high=self.force_mag, shape=(1,))
        
        s_low = np.array([ -4.80000000e+00,-3.40282347e+38,-4.18879020e-01,-3.40282347e+38])
        s_high= np.array([4.80000000e+00, 3.40282347e+38, 4.18879020e-01,3.40282347e+38])
        self.observation_space = spaces.Box(low=-s_low,
                high = s_high)

        # Angle at which to fail the episode
        self.theta_threshold_radians = 12 * 2 * math.pi / 360
        self.x_threshold = 2.4
        self.goal =  np.zeros(4) #np.array([1.0, 0.0, self.theta_threshold_radians, 0.0])

        # Angle limit set to 2 * theta_threshold_radians so failing observation is still within bounds
        high = np.array([
            self.x_threshold * 2,
            np.finfo(np.float32).max,
            self.theta_threshold_radians * 2,
            np.finfo(np.float32).max])

        #self.action_space = spaces.Discrete(2)
        #self.observation_space = spaces.Box(-high, high)

        self.viewer = None
        self.state = None

        self.steps_beyond_done = None

        self.S = np.eye(4)
        self.S[0,0] = 100.
        self.S[2,2] = 1.
        self.S[3,3] = 100.
        self.A = np.eye(1)*0.01

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self, action):
        #assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))
        state = self.state
        x, x_dot, theta, theta_dot = state
        force = action[0] #self.force_mag if action==1 else -self.force_mag
        step_cost = self.cost(state, action)

        costheta = math.cos(theta)
        sintheta = math.sin(theta)
        temp = (force + self.polemass_length * theta_dot * theta_dot * sintheta) / self.total_mass
        thetaacc = (self.gravity * sintheta - costheta* temp) / (self.length * (4.0/3.0 - self.masspole * costheta * costheta / self.total_mass))
        xacc  = temp - self.polemass_length * thetaacc * costheta / self.total_mass
        x  = x + self.tau * x_dot
        x_dot = x_dot + self.tau * xacc
        theta = theta + self.tau * theta_dot

        if theta > 2*math.pi:
            theta = theta - np.floor(theta/2./math.pi)*2.*math.pi
        elif theta < -2*math.pi:
            theta = theta + np.floor(np.abs(theta)/2./math.pi)*2*math.pi

        theta_dot = theta_dot + self.tau * thetaacc
        self.state = (x,x_dot,theta,theta_dot)
        done =  x < -self.x_threshold \
                or x > self.x_threshold \
                or theta < -self.theta_threshold_radians \
                or theta > self.theta_threshold_radians
        done = bool(done)
        done = False

        return np.array(self.state), step_cost, done, {}

    def cost(self, obs, action):
        assert action.ndim == 1
        state = obs
        return (0.5*(state-self.goal).dot(self.S).dot(state-self.goal) + 0.5*action.dot(self.A).dot(action))


    def eval(self, X, U, C = None): 
        assert X.ndim == 2 and U.ndim == 2
        dX = X.shape[1]
        dU = U.shape[1]
        T = X.shape[0]
        
        #gradients and Hessians of the previous cost function
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])
        
        lx = (X-np.tile(self.goal,(T,1))).dot(self.S.T)
        lu = U.dot(self.A.T)
        luu = np.array([self.A]*T)
        lxx = np.array([self.S]*T)
        lux = np.array([np.zeros((dU,dX))]*T)

        return l, lx, lu, lxx, luu, lux
    

    def _reset(self):
        self.state = np.random.uniform(low=-0.01, high=0.01, size=(4,))
        self.steps_beyond_done = None
        return np.array(self.state)

    
