"""multi-link swimmer moving in a fluid."""

import numpy as np
from GeneralTools import rk4, cartesian
from gym import spaces
from gym.utils import seeding
from gym.spaces import Box
#from IPython import embed

__copyright__ = "Copyright 2013, RLPy http://acl.mit.edu/RLPy"
__credits__ = ["Alborz Geramifard", "Robert H. Klein", "Christoph Dann",
               "William Dabney", "Jonathan P. How"]
__license__ = "BSD 3-Clause"
__author__ = "Christoph Dann"


class Swimmer(object):

    """
    A swimmer consisting of a chain of d links connected by rotational joints.
    Each joint is actuated. The goal is to move the swimmer to a specified goal
    position.

    *States*:
        | 2 dimensions: position of nose relative to goal
        | d -1 dimensions: angles
        | 2 dimensions: velocity of the nose
        | d dimensions: angular velocities

    *Actions*:
        each joint torque is discretized in 3 values: -2, 0, 2

    .. note::
        adapted from Yuval Tassas swimmer implementation in Matlab available at
        http://www.cs.washington.edu/people/postdocs/tassa/code/

    .. seealso::
        Tassa, Y., Erez, T., & Smart, B. (2007).
        *Receding Horizon Differential Dynamic Programming.*
        In Advances in Neural Information Processing Systems.
    """
    dt = 0.01#0.02 #0.03
    #episodeCap = 1000
    #discount_factor = 0.98

    def __init__(self, d=3, k1=7.5, k2=0.3):
        """
        d:
            number of joints
        """
        self.d = d
        self.k1 = k1
        self.k2 = k2
        self.nose = 0
        self.masses = np.ones(d)
        self.lengths = np.ones(d)
        self.inertia = self.masses * self.lengths * self.lengths / 12.
        self.goal = np.zeros(2)

        # reward function parameters
        self.cu = 1e-5# 0.001
        self.cx = 4# 2.

        
        '''
        self.Q = np.zeros((2+d-1+2+d, 4+2*d-1))
        self.Q[0:self.goal.shape[0],0:self.goal.shape[0]] = np.eye(self.goal.shape[0])*self.cx
        self.R = np.eye(d-1)*self.cu
        self.offset = np.zeros(4+2*d-1)
        self.offset[0:self.goal.shape[0]] = self.goal
        '''

        self.vy_loc = 6
        self.target_speed = -1
        self.Q = np.zeros((2+d+2+d-1,2+d+2+d-1))
        self.Q[0:2, 0:2] = np.eye(d-1)*self.cx
        #self.Q[self.vy_loc,self.vy_loc] = self.cx
        self.R = np.eye(d-1)*self.cu
        self.offset = np.zeros(2+d+2+d-1)
        #self.offset[self.vy_loc] = self.target_speed #vy* = 2
        self.offset[0:2] = self.goal

        #print self.Q, self.R
        Q = np.eye(self.d, k=1) - np.eye(self.d)
        Q[-1, :] = self.masses
        A = np.eye(self.d, k=1) + np.eye(self.d)
        A[-1, -1] = 0.
        self.P = np.dot(np.linalg.inv(Q), A * self.lengths[None, :]) / 2.

        self.U = np.eye(self.d) - np.eye(self.d, k=-1)
        self.U = self.U[:, :-1]
        self.G = np.dot(self.P.T * self.masses[None, :], self.P)

        # incidator variables for angles in a state representation
        self.angles = np.zeros(2 + self.d * 2 + 1, dtype=np.bool)
        self.angles[2:2 + self.d - 1] = True
        self.angles[-self.d - 2:] = True

        self.actions = cartesian((d - 1) * [[-2., 0., 2]])
        self.actions_num = len(self.actions)

        
        self.statespace_limits = [[-15, 15]] * 2 + [[-np.pi, np.pi]] * (d - 1) \
            + [[-2, 2]] * 2 + [[-np.pi * 2, np.pi * 2]] * d
        self.statespace_limits = np.array(self.statespace_limits)
        self.continuous_dims = range(self.statespace_limits.shape[0])

        
        s_low = np.array([-15]*2 + [-np.pi]*(d-1) + [-2]*2 + [-np.pi*2]*d)
        s_high = np.array([15]*2 + [np.pi]*(d-1) + [2]*2 + [2*np.pi]*d)
        self.observation_space = spaces.Box(low=-s_low,high = s_high)
        self.action_space = spaces.Box(low=-2., high=2., shape=(d-1,))
        '''
        s_low = np.array([-15]*2 + [-np.pi]*(d) + [-2]*2 + [-np.pi*2]*d)
        s_high = np.array([15]*2 + [np.pi]*(d) + [2]*2 + [2*np.pi]*d)
        self.observation_space = spaces.Box(low=-s_low,high = s_high)
        self.action_space = spaces.Box(low=-2., high=2., shape=(d-1,))
        '''
    def reset(self):
        self.theta = np.zeros(self.d)
        #self.theta = np.ones(self.d)*np.pi/2.
        self.pos_cm = np.array([0,3]) + np.random.randn(2)*1e-1
        self.v_cm = np.zeros(2) + np.random.randn(2)*1e-1
        self.dtheta = np.zeros(self.d) + np.random.randn(self.d)*1e-1
        return self.obs

    @property
    def obs(self):
        return np.hstack(self._body_coord())
        ang = np.mod(self.theta, 2*np.pi) - np.pi
        return np.hstack((self.pos_cm, ang, self.v_cm, self.dtheta))

    def isTerminal(self):
        return False

    def _body_coord(self):
        """
        transforms the current state into coordinates that are more
        reasonable for learning
        returns a 4-tupel consisting of:
        nose position, joint angles (d-1), nose velocity, angular velocities

        The nose position and nose velocities are referenced to the nose rotation.
        """
        cth = np.cos(self.theta)
        sth = np.sin(self.theta)
        M = self.P - 0.5 * np.diag(self.lengths)
        #  stores the vector from the center of mass to the nose
        c2n = np.array([np.dot(M[self.nose], cth), np.dot(M[self.nose], sth)])
        #  absolute position of nose
        T = -self.pos_cm - c2n - self.goal
        #  rotating coordinate such that nose is axis-aligned (nose frame)
        #  (no effect when  \theta_{nose} = 0)
        c2n_x = np.array([cth[self.nose], sth[self.nose]])
        c2n_y = np.array([-sth[self.nose], cth[self.nose]])
        Tcn = np.array([np.sum(T * c2n_x), np.sum(T * c2n_y)])

        #  velocity at each joint relative to center of mass velocity
        vx = -np.dot(M, sth * self.dtheta)
        vy = np.dot(M, cth * self.dtheta)
        #  velocity at nose (world frame) relative to center of mass velocity
        v2n = np.array([vx[self.nose], vy[self.nose]])
        #  rotating nose velocity to be in nose frame
        Vcn = np.array([np.sum((self.v_cm + v2n) * c2n_x),
                        np.sum((self.v_cm + v2n) * c2n_y)])
        #  angles should be in [-pi, pi]
        ang = np.mod(
            self.theta[1:] - self.theta[:-1] + np.pi,
            2 * np.pi) - np.pi
        return Tcn, ang, Vcn, self.dtheta
        

    def step(self, a):
        d = self.d
        #print a
        step_cost = self.cost(self.obs, a)

        s = np.hstack((self.pos_cm, self.theta, self.v_cm, self.dtheta))
        ns = rk4(
            dsdt, s+np.random.randn(s.shape[0])*1e-3, [0,
                      self.dt], a+np.random.randn(a.shape[0])*1e-7, self.P, self.inertia, self.G, self.U, self.lengths,
            self.masses, self.k1, self.k2)[-1]

        self.theta = ns[2:2 + d]
        self.v_cm = ns[2 + d:4 + d]
        self.dtheta = ns[4 + d:]
        self.pos_cm = ns[:2]
        
        #return self.obs, step_cost, False, {"reach_goal":np.linalg.norm(self.obs[0:2] - self.goal)<1.}
        return self.obs, step_cost, False, {"reach_goal":np.abs(
                self.obs[self.vy_loc] - self.target_speed)<self.target_speed/2.}
    


    def cost(self, obs, a):
        """
        penalizes the l2 distance to the goal (almost linearly) and
        a small penalty for torques coming from actions
        """

        v1 = 0.5*((obs-self.offset).dot(self.Q).dot(obs-self.offset))\
                + 0.5*a.dot(self.R).dot(a)

        #return 0.5*(obs-self.offset).dot(self.Q).dot(obs-self.offset)\
        #        + 0.5*a.dot(self.R).dot(a)

        #xrel = self._body_coord()[0] - self.goal
        #xrel = obs[0:self.goal.shape[0]] - self.goal
        #dist = xrel.dot(xrel) #np.sum(xrel ** 2)
        #v2 = 0.5*self.cx * dist + 0.5*self.cu * a.dot(a)
        #print v1-v2
        return v1

    def eval(self, X, U, C = None):
        assert X.ndim == 2 and U.ndim == 2
        dX = X.shape[1]
        dU = U.shape[1]
        T = X.shape[0]

        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])
        lx = (X - np.tile(self.offset, (T,1))).dot(self.Q)
        lu = U.dot(self.R)
        luu = np.array([self.R]*T)
        lxx = np.array([self.Q]*T) #+ 1e-8*np.eye(self.Q.shape[0])
        lux = np.array([np.zeros((dU,dX))]*T)

        return l, lx, lu, lxx, luu, lux


    def seed(self,seed):
        np.random.seed(seed)


def dsdt(s, t, a, P, I, G, U, lengths, masses, k1, k2):
    """
    time derivative of system dynamics
    """
    d = len(a) + 1
    theta = s[2:2 + d]
    vcm = s[2 + d:4 + d]
    dtheta = s[4 + d:]

    cth = np.cos(theta)
    sth = np.sin(theta)
    rVx = np.dot(P, -sth * dtheta)
    rVy = np.dot(P, cth * dtheta)
    Vx = rVx + vcm[0]
    Vy = rVy + vcm[1]

    Vn = -sth * Vx + cth * Vy
    Vt = cth * Vx + sth * Vy

    EL1 = np.dot((v1Mv2(-sth, G, cth) + v1Mv2(cth, G, sth)) * dtheta[None, :]
                 + (v1Mv2(cth, G, -sth) + v1Mv2(sth, G, cth)) * dtheta[:, None], dtheta)
    EL3 = np.diag(I) + v1Mv2(-sth, G, -sth) + v1Mv2(cth, G, cth)
    EL2 = - k1 * np.dot((v1Mv2(-sth, P.T, -sth) + v1Mv2(cth, P.T, cth)) * lengths[None, :], Vn) \
          - k1 * np.power(lengths, 3) * dtheta / 12. \
          - k2 * \
        np.dot((v1Mv2(-sth, P.T, cth) + v1Mv2(cth, P.T, sth))
               * lengths[None, :], Vt)
    ds = np.zeros_like(s)
    ds[:2] = vcm
    ds[2:2 + d] = dtheta
    ds[2 + d] = - \
        (k1 * np.sum(-sth * Vn) + k2 * np.sum(cth * Vt)) / np.sum(masses)
    ds[3 + d] = - \
        (k1 * np.sum(cth * Vn) + k2 * np.sum(sth * Vt)) / np.sum(masses)
    ds[4 + d:] = np.linalg.solve(EL3, EL1 + EL2 + np.dot(U, a))
    return ds



def v1Mv2(v1, M, v2):
    """
    computes diag(v1) dot M dot diag(v2).
    returns np.ndarray with same dimensions as M
    """
    return v1[:, None] * M * v2[None, :]
