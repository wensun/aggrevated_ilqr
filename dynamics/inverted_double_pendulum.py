import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env
from IPython import embed


class Customized_InvertedDoublePendulumEnv(mujoco_env.MujocoEnv, utils.EzPickle):
    '''
    modify: 
    1. initilization: from 1 to 0.5
    2. include site_xpos in the observation
    '''

    def __init__(self):
        mujoco_env.MujocoEnv.__init__(self, 'inverted_double_pendulum.xml', 5)
        utils.EzPickle.__init__(self)

        #self.Q = np.zeros((self.observation_space.shape[0], self.observation_space.shape[0]))
        #self.R = np.zeros((self.action_space.shape[0],self.action_space.shape[0]))


    def _step(self, action):
        self.do_simulation(action, self.frame_skip)
        ob = self._get_obs()
        x, _, y = self.model.data.site_xpos[0]
        dist_penalty = 0.01 * x ** 2 + (y - 2) ** 2
        v1, v2 = self.model.data.qvel[1:3]
        vel_penalty = 1e-3 * v1**2 + 5e-3 * v2**2
        #alive_bonus = 10
        #r = (alive_bonus - dist_penalty - vel_penalty)[0]
        action_penalty = 1e-4*action.dot(action)
        c = dist_penalty + vel_penalty + action_penalty
        done = False #bool(y <= 1)
        return ob, 10.*c[0], done, {}

    def cost(self, obs, a):
        x = obs[0]
        y = obs[2]
        v1 = obs[-5]
        v2 = obs[-4]
        dist_penalty = 0.01 * x ** 2 + (y - 2) ** 2
        vel_penalty = 1e-3 * v1**2 + 5e-3 * v2**2
        action_penalty = 1e-4*a.dot(a)
        return 10.*dist_penalty + 10.*vel_penalty + 10.*action_penalty

    def eval(self, X, U, C = None):
        T,dX = X.shape
        dU = U.shape[1]

        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i],U[i])
        
        offset = np.zeros(self.observation_space.shape[0])
        offset[2] = 2.

        H = np.zeros((dX, dX))
        R = np.eye(dU)*2*1e-4*10.
        H[0,0] = 2*0.01
        H[2,2] = 2*1.
        H[-5,-5] = 2*1e-3
        H[-4,-4] = 2*5e-3
        H = H*10. + 1e-8*np.eye(dX)

        lxx = np.array([H]*T)
        luu = np.array([R]*T)
        lux = np.array([np.zeros((dU,dX))]*T)
        lx = (X - np.tile(offset,(T,1))).dot(H)
        lu = U.dot(R)

        return l, lx, lu, lxx, luu, lux


    def _get_obs(self):
        return np.concatenate([
            self.model.data.site_xpos.T, #3D
            self.model.data.qpos[:1],  # cart x pos 1d
            np.sin(self.model.data.qpos[1:]),  # link angles 2D
            np.cos(self.model.data.qpos[1:]), #2D 
            np.clip(self.model.data.qvel, -10, 10), #3D 
            np.clip(self.model.data.qfrc_constraint, -10, 10) #3D
        ]).ravel()

    def reset_model(self):
        self.set_state(
            self.init_qpos + self.np_random.uniform(low=-.05, high=.05, size=self.model.nq),
            self.init_qvel + self.np_random.randn(self.model.nv) * .05
        )
        return self._get_obs()

    def viewer_setup(self):
        v = self.viewer
        v.cam.trackbodyid = 0
        v.cam.distance = v.model.stat.extent * 0.5
        v.cam.lookat[2] += 3  # v.model.stat.center[2]
