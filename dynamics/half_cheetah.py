import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env

class Customized_HalfCheetahEnv(mujoco_env.MujocoEnv, utils.EzPickle):
    def __init__(self):
        self.du = 6
        self.vx_target = 2.
        self.dqpos = 9-1
        self.dqvel = 9

        self.offset = np.zeros(self.dqpos+self.dqvel)
        self.offset[self.dqpos] = self.vx_target

        self.Q = np.zeros((self.dqpos + self.dqvel, self.dqpos+self.dqvel))
        self.Q[self.dqpos, self.dqpos] = 1.
        self.R = np.eye(self.du)*1e-4
        mujoco_env.MujocoEnv.__init__(self, 'half_cheetah.xml', 5)
        utils.EzPickle.__init__(self)


    def _step(self, a):
        #self.do_simulation(action, self.frame_skip)
        curr_ob = self._get_obs()
        step_cost = self.cost(curr_ob, a)
        self.do_simulation(a, 1)
        ob = self._get_obs()
        done = False
        return ob, step_cost, done, dict()

    def cost(self, obs, a):
        v1 = 0.5*((obs-self.offset).dot(self.Q).dot(obs-self.offset))\
                + 0.5*a.dot(self.R).dot(a)
        return v1

    def eval(self, X, U, C = None):  
        assert X.ndim == 2 and U.ndim == 2
        dX = X.shape[1]
        dU = U.shape[1]
        T = X.shape[0]
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])
        lx = (X - np.tile(self.offset, (T,1))).dot(self.Q)
        lu = U.dot(self.R)
        luu = np.array([self.R]*T)
        lxx = np.array([self.Q]*T) #+ 1e-8*np.eye(self.Q.shape[0])
        lux = np.array([np.zeros((dU,dX))]*T)
        return l, lx, lu, lxx, luu, lux


    def _get_obs(self):
        return np.concatenate([
            self.model.data.qpos.flat[1:],
            self.model.data.qvel.flat,
        ])

    def reset_model(self):
        qpos = self.init_qpos + self.np_random.uniform(low=-.01, high=.01, size=self.model.nq)
        qvel = self.init_qvel + self.np_random.uniform(low=-.0, high=.01, size=self.model.nv)#self.np_random.randn(self.model.nv) * .02
        self.set_state(qpos, qvel)
        return self._get_obs()

    def viewer_setup(self):
        self.viewer.cam.distance = self.model.stat.extent * 0.5
