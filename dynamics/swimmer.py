import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env

class Customized_SwimmerEnv(mujoco_env.MujocoEnv, utils.EzPickle):
    '''
    what we modified:
    1. initial reset: from 0.1 to 0.01
    2. _get_obs: returns full qpos and qvel
    '''
    def __init__(self):
        self.du = 2
        self.dqpos = 5 #self.model.data.qpos.shape[0]
        self.dqvel = 5 #self.model.data.qvel.shape[0]
        self.vx_target = 2. 
        self.offset = np.zeros(self.dqpos + self.dqvel)
        self.offset[self.dqpos] = self.vx_target

        self.Q = np.zeros((self.dqpos + self.dqvel, self.dqpos+self.dqvel))
        self.Q[self.dqpos, self.dqpos] = 1.
        self.R = np.eye(self.du)*1e-4

        #print "down"
        mujoco_env.MujocoEnv.__init__(self, 'swimmer.xml', 4)
        utils.EzPickle.__init__(self)

    def _step(self, a):
        curr_ob = self._get_obs()
        step_cost = self.cost(curr_ob, a)
        #self.do_simulation(a, self.frame_skip)
        self.do_simulation(a, 1)
        ob = self._get_obs()
        #return ob, reward, False, dict(reward_fwd=reward_fwd, reward_ctrl=reward_ctrl)
        return ob, step_cost, False, dict() #to make it cost

    def _get_obs(self):
        qpos = self.model.data.qpos
        qvel = self.model.data.qvel
        return np.concatenate([qpos.flat, qvel.flat])
        #return np.concatenate([qpos.flat[2:], qvel.flat])

    def cost(self, obs, a):
        v1 = 0.5*((obs-self.offset).dot(self.Q).dot(obs-self.offset))\
                + 0.5*a.dot(self.R).dot(a)
        return v1

    def eval(self, X, U, C = None):
        assert X.ndim == 2 and U.ndim == 2
        dX = X.shape[1]
        dU = U.shape[1]
        T = X.shape[0]

        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])
        lx = (X - np.tile(self.offset, (T,1))).dot(self.Q)
        lu = U.dot(self.R)
        luu = np.array([self.R]*T)
        lxx = np.array([self.Q]*T) #+ 1e-8*np.eye(self.Q.shape[0])
        lux = np.array([np.zeros((dU,dX))]*T)

        return l, lx, lu, lxx, luu, lux


    def reset_model(self):
        self.set_state(
            self.init_qpos + self.np_random.uniform(low=-.02, high=.02, size=self.model.nq),
            self.init_qvel + self.np_random.uniform(low=-.02, high=.02, size=self.model.nv)
        )
        return self._get_obs()
