import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env
from IPython import embed
import numpy as np

class Customized_ReacherEnv(mujoco_env.MujocoEnv, utils.EzPickle):

    '''
    compare to the original Reacher from openAI Gym, we: 
    1. reduce the initialization in qpos from 0.1 to 0.01
    reduce goal from 0.2 to 0.05
    2. construct a cost (negative reward) function that only depends on state (observation), and action.
    3. we set cost to square distance to the goal instead of l2_norm (which is non-convex)
    '''

    def __init__(self):
        utils.EzPickle.__init__(self)
        mujoco_env.MujocoEnv.__init__(self, 'reacher.xml', 2)
        

    def _step(self, a):
        vec = self.get_body_com("fingertip")-self.get_body_com("target")
        reward_dist = - 10*vec.dot(vec)   #np.linalg.norm(vec)
        reward_ctrl = - 0.1*np.square(a).sum()
        reward = reward_dist + reward_ctrl
        step_cost = -reward
 
        self.do_simulation(a, self.frame_skip)
        ob = self._get_obs()
        done = False
        return ob, step_cost, done, dict(reward_dist=reward_dist, reward_ctrl=reward_ctrl)

    def cost(self, obs, a):
        vec = obs[-2:] #difference between fingertip and target
        cost_dist = 100*vec.dot(vec)   #np.linalg.norm(vec)
        cost_ctrl = 0.1*np.square(a).sum()
        cost = cost_dist + cost_ctrl
        return cost

    def eval(self, X, U, C = None):  #coresponds to the cost function above. 
        goal_dim = 2
        tmpA = np.zeros((goal_dim, X.shape[1]))
        tmpA[:,-goal_dim:] = np.eye(goal_dim)
        
        assert X.ndim == 2 and U.ndim == 2 #trajectory_wise
        T, dX = X.shape
        dU = U.shape[1]

        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i], U[i])

        lx = 100*2*X.dot(tmpA.T.dot(tmpA))
        lu = 0.1*2*U
        luu = np.array([0.1*2*np.eye(dU)]*T)
        lxx = np.array([100*2*tmpA.T.dot(tmpA)]*T) + 1e-8*np.eye(dX)
        lux = np.array([np.zeros((dU,dX))]*T)

        return l, lx, lu, lxx, luu, lux

    '''
    def quadratize_cost(self, obs_bar, a_bar):
        #quadratize the cost at obs_bar and a_bar:
        goal_dim = 3
        tmpA = np.zeros((goal_dim, obs_bar.shape[0]))
        tmpA[:,-goal_dim:] = np.eye(goal_dim)

        C_xx = 2*(tmpA.transpose().dot(tmpA) + np.eye(obs_bar.shape[0])*0)
        C_uu = 2*np.eye(a_bar.shape[0])
        C_ux = np.zeros((a_bar.shape[0], obs_bar.shape[0]))
        C_u = np.zeros(a_bar.shape[0])
        C_x = np.zeros(obs_bar.shape[0])
        C = 0.      
        
        return C_xx, C_uu, C_ux, C_u, C_x, C
        
    def test_quadratization(self):
        obs_bar= np.random.randn(11)
        a_bar = np.random.randn(2)
        test_obs = obs_bar + np.random.rand(11)*987
        test_a = a_bar + np.random.rand(2)*987
        true_c = self.cost(test_obs,test_a)
        C_xx,C_uu,C_ux,C_u, C_x, C = self.quadratize_cost(obs_bar, a_bar)
        app_c = 0.5*test_obs.dot(C_xx).dot(test_obs) \
                +0.5*test_a.dot(C_uu).dot(test_a)\
                +test_a.dot(C_ux).dot(test_obs)\
                +C_u.dot(test_a)\
                +C_x.dot(test_obs) + C
        print app_c - true_c
        print app_c, true_c
    '''



    def viewer_setup(self):
        self.viewer.cam.trackbodyid = 0

    def reset_model(self):
        qpos = self.np_random.uniform(low=-0.01, high=0.01, size=self.model.nq) + self.init_qpos
        while True:
            self.goal = self.np_random.uniform(low=-.05, high=.05, size=2)
            if np.linalg.norm(self.goal) < 2:
                break
        qpos[-2:] = self.goal
        qvel = self.init_qvel + self.np_random.uniform(low=-.005, high=.005, size=self.model.nv)
        qvel[-2:] = 0
        self.set_state(qpos, qvel)
        return self._get_obs()

    def _get_obs(self):
        theta = self.model.data.qpos.flat[:2]
        return np.concatenate([
            np.cos(theta),
            np.sin(theta),
            self.model.data.qpos.flat[2:],
            self.model.data.qvel.flat[:2],
            (self.get_body_com("fingertip") - self.get_body_com("target"))[0:2]
        ])

