import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env

class Customized_InvertedPendulumEnv(mujoco_env.MujocoEnv, utils.EzPickle):

    '''
    customized inverted pendulumEnv from openai gym mujoco.
    we modify:
    1. cost function, that quadratically penalizes action, norm of obs, and deviation on obs[1]
    2. get rid of done, it will continue run till the end of specified time horizon. 
    '''

    def __init__(self):
        utils.EzPickle.__init__(self)
        mujoco_env.MujocoEnv.__init__(self, 'inverted_pendulum.xml', 2)
   
    def cost(self, obs, a):
        cost_ctr = np.dot(a, a)*0.01
        cost_norm = np.dot(obs,obs)*0.001
        cost_dist = (obs[1] - 0.2)**2
        return cost_dist + cost_norm + cost_ctr

    def _step(self, a):
        reward = 1.0 #if np.abs(ob[1] <= 0.2/100.) else 0
        self.do_simulation(a, self.frame_skip)
        ob = self._get_obs()
        reward = -cost(ob, a) #negative cost. 
        #notdone = np.isfinite(ob).all() and (np.abs(ob[1]) <= .2)
        #done = not notdone
        return ob, reward, done, {}

    def reset_model(self):
        qpos = self.init_qpos + self.np_random.uniform(size=self.model.nq, low=-0.01, high=0.01)
        qvel = self.init_qvel + self.np_random.uniform(size=self.model.nv, low=-0.01, high=0.01)
        self.set_state(qpos, qvel)
        return self._get_obs()

    def _get_obs(self):
        return np.concatenate([self.model.data.qpos, self.model.data.qvel]).ravel()

    def viewer_setup(self):
        v = self.viewer
        v.cam.trackbodyid = 0
        v.cam.distance = v.model.stat.extent
