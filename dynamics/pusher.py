import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env

import mujoco_py
from mujoco_py.mjlib import mjlib

class Customized_PusherEnv(mujoco_env.MujocoEnv, utils.EzPickle):
    '''
    compare to the original pusher from openAI Gym, we: 
    2. construct a cost (negative reward) function that only depends on state (observation), and action.
    3. we set cost to square distance to the goal instead of l2_norm (which is non-convex)
    '''
    
    def __init__(self):
        utils.EzPickle.__init__(self)
        mujoco_env.MujocoEnv.__init__(self, 'pusher.xml', 5)

    def _step(self, a):
        vec_1 = self.get_body_com("object") - self.get_body_com("tips_arm")
        vec_2 = self.get_body_com("object") - self.get_body_com("goal")

        reward_near = - vec_1.dot(vec_1)#np.linalg.norm(vec_1)
        reward_dist = - vec_2.dot(vec_2)#np.linalg.norm(vec_2)
        reward_ctrl = - np.square(a).sum()
        #reward = reward_dist + 0.1 * reward_ctrl + 0.5 * reward_near
        reward = 0.5*16.*reward_dist + 0.1 * reward_ctrl + 0.5 * reward_near

        self.do_simulation(a, self.frame_skip)
        ob = self._get_obs()
        done = False
        return ob, -reward, done, dict(
                reward_dist=reward_dist,
                reward_ctrl=reward_ctrl,
                hit = (np.linalg.norm(vec_1) < 0.1),
                reach_goal = (np.linalg.norm(vec_2) < 0.08) #0.08 is the radius of the goal region.
                )

    '''
    def cost(self, obs ,a):
        #this obs here matches to the return _get_obs function
        all_pos = obs[-9:]
        tip = all_pos[0:3]
        obj = all_pos[3:6]
        goal = all_pos[6:]
        vec_1 = obj - tip
        vec_2 = obj - goal

        cost_near = vec_1.dot(vec_1)
        cost_dist = vec_2.dot(vec_2)
        cost_ctrl = np.square(a).sum()
        cost = cost_dist + 0.1 * cost_ctrl + 0.5 * cost_near
        return cost
    
    def eval(self, X, U, C = None): #coresponds to the cost function above. 
        goal_dim = 3
        obj_dim = 3
        I_o = np.zeros((obj_dim, X.shape[1]))
        I_t = np.zeros((obj_dim, X.shape[1]))
        I_g = np.zeros((goal_dim, X.shape[1]))
        I_t[:, -9:-6] = np.eye(obj_dim)
        I_o[:, -6:-3] = np.eye(obj_dim)
        I_g[:,-3:] = np.eye(goal_dim)

        assert X.ndim == 2 and U.ndim == 2
        T,dX = X.shape
        dU = U.shape[1]
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i],U[i])
        lxx = np.array([(I_o - I_t).T.dot(I_o - I_t) + 2*(I_o-I_g).T.dot(I_o-I_g)]*T)
        luu = np.array([0.2*np.eye(dU)]*T)
        lux = np.array([np.zeros((dU,dX))]*T)
        lx = X.dot((I_o - I_t).T.dot(I_o - I_t) + 2*(I_o-I_g).T.dot(I_o-I_g))
        lu = 0.2*U

        return l, lx, lu, lxx, luu, lux
    '''

    def viewer_setup(self):
        self.viewer.cam.trackbodyid = -1
        self.viewer.cam.distance = 4.0

    def reset_model(self):
        qpos = self.init_qpos

        qpos[0:-4] += np.random.randn(qpos[0:-4].shape[0])*0.00001

        self.goal_pos = np.asarray([0, 0])
        while True:
            self.cylinder_pos = np.concatenate([
                    self.np_random.uniform(low=-0.1, high=0, size=1), #change from high=0. to high=0.11
                    self.np_random.uniform(low=-0.1, high=0.1, size=1)]) #change from 0.2 to 0.02
            if np.linalg.norm(self.cylinder_pos - self.goal_pos) > 0.1:  #0.17
                break

        qpos[-4:-2] = self.cylinder_pos
        qpos[-2:] = self.goal_pos
        qvel = self.init_qvel + self.np_random.uniform(low=-0.005,
                high=0.005, size=self.model.nv)
        qvel[-4:] = 0
        self.set_state(qpos, qvel)
        return self._get_obs()

    '''
    def _get_obs(self):
        return np.concatenate([
            self.model.data.qpos.flat[:-4],
            #self.model.data.qpos.flat[:7],
            self.model.data.qvel.flat[:-4],
            #self.model.data.qvel.flat[:7],
            self.get_body_com("tips_arm"), #3d
            self.get_body_com("object"),   #3d
            self.get_body_com("goal"),     #3d
        ])
    '''

    def _get_obs(self):
        tip_pos = self.get_body_com("tips_arm")
        obj_pos = self.get_body_com("object")
        goal_pos= self.get_body_com("goal")
        
        return np.concatenate([
            self.model.data.qpos.flat[:-4],
            self.model.data.qvel.flat[:-4],
            obj_pos - tip_pos, #near 
            obj_pos - goal_pos, #dist
        ])

    def cost(self, obs, a):
        obj_goal = obs[-3:]  #dist
        obj_tip = obs[-6:-3] #near

        #return 0.5*obj_tip.dot(obj_tip) + obj_goal.dot(obj_goal) + 0.1*a.dot(a)
        return 0.5*obj_tip.dot(obj_tip) + 0.5*16.*obj_goal.dot(obj_goal) + 0.1*a.dot(a)
    
    def eval(self, X, U, C = None):
        tmpA = np.zeros((6, X.shape[1]))
        tmpA[:3, -6:-3] = np.eye(3)
        tmpA[-3:, -3:] = 4.*np.eye(3) 
        
        T,dX = X.shape
        dU = U.shape[1]
        
        l = np.zeros(T)
        for i in xrange(T):
            l[i] = self.cost(X[i],U[i])
        
        lxx = np.array([tmpA.T.dot(tmpA)]*T) + 0.*np.eye(X.shape[1])
        luu = np.array([0.2*np.eye(dU)]*T)
        lux = np.array([np.zeros((dU,dX))]*T)
        lx = X.dot(tmpA.T.dot(tmpA))
        lu = 0.2*U

        return l, lx, lu, lxx, luu, lux
