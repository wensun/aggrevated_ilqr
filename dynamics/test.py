import numpy as np
from reacher import Customized_ReacherEnv
from pusher import Customized_PusherEnv
from striker import Customized_StrikerEnv
from inverted_double_pendulum import Customized_InvertedDoublePendulumEnv
#env = Customized_ReacherEnv()
env = Customized_PusherEnv()
env = Customized_InvertedDoublePendulumEnv()
#env = Customized_StrikerEnv()
dX = env.observation_space.shape[0]
dU = env.action_space.shape[0]

X = np.random.randn(100, dX)
U = np.random.randn(100, dU)

l,lx,lu,lxx,luu,lux = env.eval(X,U)
X_test = X + 1e1*np.random.randn(100,dX)
U_test = U + 1e1*np.random.randn(100,dU)

def test_cost(l,lx,lu,lxx,luu,lux, X_test,U_test, X,U):
    error = 0
    for i in xrange(X_test.shape[0]):
        x = X_test[i]
        u = U_test[i]
        c_r = env.cost(x,u)
        c_p = l[i] + (x-X[i]).dot(lx[i]) + (u-U[i]).dot(lu[i]) \
                + (u-U[i]).dot(lux[i]).dot(x-X[i]) \
                + 0.5*(x-X[i]).dot(lxx[i]).dot(x-X[i])\
                + 0.5*(u-U[i]).dot(luu[i]).dot(u-U[i])
        error += np.abs(c_r - c_p)

    return error / (X_test.shape[0])

err = test_cost(l, lx,lu,lxx,luu,lux,X_test,U_test,X,U)
print err
