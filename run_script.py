import os
import numpy as np
import argparse,sys

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--env",required=True)
args,_ = parser.parse_known_args([arg for arg in sys.argv[1:] if arg not in ('-h', '--help')])

np.random.seed(10)
random_seeds = np.array(np.random.rand(20)*(2**32-1), dtype = int)
print random_seeds


env = args.env #"CartPole"
#env = "Acrobot"
#env = "InvertedDoublePendulumEnv"
#env = "Reacher"
#env = "HalfCheetah"
#env = "Hopper"
#env = "Helicopter_Hover"
#env = "Helicopter_Funnel"
#env = "Pusher"
#env = "Striker"
#env = "Swimmer"
agent = "modular_rl.agentzoo.TrpoAgent"

if env == "CartPole":
    T = 100 #length of a trajectory
    N = 10 #num of trajectories in each batch
    gamma = 1. #discout factor
    seed = 10  #random seed
    trpo_kl = 0.005 #gradient descent trust region size
    kvs1 = 5 # num of trust region gradient descent steps per optimal control call
    history_k = 5 #use previous history_k batch of trajectories to learn dynamics and linearize nn policy
    kl_step = 0.1
    learn_cost_flag = 0
    filt = 0
    n_iter = 100

elif env == "Acrobot":
    T = 100 #length of a trajectory
    N = 50 #num of trajectories in each batch
    gamma = 0.99 #discout factor
    seed = 10  #random seed
    trpo_kl = 0.005 #gradient descent trust region size
    kvs1 = 5 # num of trust region gradient descent steps per optimal control call
    history_k = 2 #use previous history_k batch of trajectories to learn dynamics and linearize nn policy
    kl_step = 0.1
    learn_cost_flag = 0
    filt = 0
    n_iter = 100


elif env == "InvertedDoublePendulumEnv":
    T = 50
    N = 100
    gamma = 1.
    seed = 10
    trpo_kl = 0.005
    kvs1 = 5
    history_k = 5
    kl_step = 0.1
    learn_cost_flag = 0
    filt = 0
    n_iter = 100

elif env == "Reacher":
    T = 100
    N = 200  #200
    gamma = 0.995
    seed = 100
    trpo_kl = 0.005 #0.005
    kvs1 = 1 #10      
    history_k = 2  #do not use too many history runs, unstable especially at the beginning.
    kl_step = 0.01
    filt = 0
    learn_cost_flag = 0
    n_iter = 50

elif env == "Pusher":
    T = 100
    N = 200
    gamma = 0.995  #0.995
    seed = 100
    trpo_kl = 0.005   #0.01
    kvs1 = 10     #`1
    history_k = 2
    kl_step = 0.1  #0.05
    filt = 0 #filter has to be zero here, as the hessian and gradient of the cost functions are analytic here. 
    learn_cost_flag = 0
    n_iter = 100

elif env == "Striker":
    T = 100
    N = 100
    gamma = 1.  #0.995
    seed = 100
    trpo_kl = 0.01
    kvs1 = 1
    history_k = 5
    kl_step = 0.05
    filt = 0 
    learn_cost_flag = 0

elif env == "Helicopter_Hover":
    T = 50
    N = 50
    gamma = 1.#0.99
    seed = 100
    trpo_kl = 0.005 #0.0005
    kvs1 = 5
    history_k = 5
    kl_step = 0.1
    filt = 0
    learn_cost_flag = 0
    n_iter = 100

elif env == "Helicopter_Funnel":
    T = 100
    N = 50
    gamma = 1.
    seed = 100
    trpo_kl = 0.02
    kvs1 = 5
    history_k = 5
    kl_step = 0.1
    filt = 0
    learn_cost_flag = 0
    n_iter = 300

elif env == "Swimmer":
    T = 100
    N = 100#100
    gamma = 1.
    seed = 100
    trpo_kl = 0.005
    kvs1 = 5
    history_k = 5
    kl_step = 0.1 #0.2
    filt = 0
    learn_cost_flag = 0
    n_iter = 100

elif env == "HalfCheetah":
    T = 100
    N = 200#100
    gamma = 0.995
    trpo_kl = 0.005
    kvs1 = 5
    history_k = 2
    kl_step = 0.2 #0.2
    filt = 0
    learn_cost_flag = 0
    n_iter = 100

elif env == "Hopper":
    T = 100
    N = 200#100
    gamma = 0.995
    trpo_kl = 0.005
    kvs1 = 5
    history_k = 2
    kl_step = 0.2 #0.2
    filt = 0
    learn_cost_flag = 0
    n_iter = 100


for repeat in range(0, len(random_seeds)):
#for repeat in xrange(1):
    seed =  random_seeds[repeat] #3216088185 # random_seeds[repeat]
    print repeat, seed
    aggrevated_ilqr_command = "./run_aggrevated_ilqr.py --env {} \
        --agent {} --T {} --seed {} --timesteps_per_batch {} --learn_cost {} \
        --gamma {} --max_kl {} --k_vs_1 {} --history_k {} --kl_step {} --filter {} --n_iter {}".format(
                env, agent, T, seed, T*N, learn_cost_flag,
                gamma, trpo_kl, kvs1,history_k,kl_step, filt, n_iter)

    print aggrevated_ilqr_command
    os.system(aggrevated_ilqr_command)

    #gae_lam = 1.
    #trpo_command = "./run_trpo.py --env {} \
    #    --agent {} --timestep_limit {} --seed {} --timesteps_per_batch {} \
    #    --gamma {} --lam {} --n_iter {}".format(env, agent, T, seed, T*N, gamma,gae_lam, n_iter)
    #gae_lam = 0.97
    #trpo_gae_command = "./run_trpo.py --env {} \
    #    --agent {} --timestep_limit {} --seed {} --timesteps_per_batch {} \
    #    --gamma {} --lam {} --n_iter {}".format(env, agent, T, seed, T*N, gamma, gae_lam, n_iter)

    #print trpo_gae_command
    #os.system(trpo_command)
    #os.system(trpo_gae_command)




