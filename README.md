# README #

Implementation of AggreVaTeD with iLQR as the guiding expert. 



### Set Up ###
* python 2.7
* Install OpenAI Gym (0.9.3)
* Install Keras (2.0.2) and use theano as the backend
* Install mujoco-py (0.5.7) following instructions from https://github.com/openai/mujoco-py/tree/0.5

### test ###

* run: python run_script.py --env='CartPole'

(environments used in the paper: CartPole, Swimmer, Hopper, HalfCheetah, Helicopter_Funnel, Helicopter_Hover)	

* for discrete MDP experiments, go to this branch: https://bitbucket.org/wensun/aggrevated_ilqr/src/discrete_MDP/

* for robust control experiments, go to this branch: https://bitbucket.org/wensun/aggrevated_ilqr/src/robust_control/




