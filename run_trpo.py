#!/usr/bin/env python
import random
from gym.envs import make
from modular_rl import *
import argparse, sys, cPickle
from tabulate import tabulate
import shutil, os, logging
import gym
from dynamics import cartpole_gym, reacher, Swimmer_rlpy, helicopter, pusher,swimmer
from dynamics import half_cheetah
from dynamics import hopper


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    update_argument_parser(parser, GENERAL_OPTIONS)
    parser.add_argument("--env",required=True)
    parser.add_argument("--agent",required=True)
    parser.add_argument("--seed", type = int, default = 1000)

    args,_ = parser.parse_known_args([arg for arg in sys.argv[1:] if arg not in ('-h', '--help')])
    if args.env == "CartPole":
        env = cartpole_gym.CartPoleEnv()
    elif args.env == "Helicopter_Hover":
        env = helicopter.Helicopter_Hover()
    elif args.env == "Helicopter_Funnel":
        env = helicopter.Helicopter_Funnel()
    elif args.env == "Reacher":
        env = reacher.Customized_ReacherEnv()
    elif args.env == "Swimmer":
        #env = Swimmer_rlpy.Swimmer()
        env = swimmer.Customized_SwimmerEnv()
    elif args.env == "HalfCheetah":
        env = half_cheetah.Customized_HalfCheetahEnv()
    elif args.env == "Hopper":
        env = hopper.Customized_HopperEnv()
    elif args.env == "Pusher":
        env = pusher.Customized_PusherEnv()
    
    #env = make(args.env)
    #env_spec = env.spec
    mondir = args.outfile + ".dir"
    agent_ctor = get_agent_cls(args.agent)
    update_argument_parser(parser, agent_ctor.options)
    args = parser.parse_args()
    if args.timestep_limit == 0:
        args.timestep_limit = env_spec.timestep_limit
    cfg = args.__dict__
    np.random.seed(args.seed)
    env.seed(args.seed)
    random.seed(args.seed)
    agent = agent_ctor(env.observation_space, env.action_space, cfg)
    if args.use_hdf:
        hdf, diagnostics = prepare_h5_file(args)
    gym.logger.setLevel(logging.WARN)

    COUNTER = 0
    def callback(stats, iter):
        global COUNTER
        COUNTER += 1
        # Print stats
        print "*********** Iteration %i ****************" % COUNTER
        print tabulate(filter(lambda (k,v) : np.asarray(v).size==1, stats.items())) #pylint: disable=W0110
       
    all_stats = run_policy_gradient_algorithm(env, agent, callback=callback, usercfg = cfg)
    result_file_name = "results/{}_{}_{}_{}".format(cfg["env"], args.seed, "trpo", cfg["lam"])  
    cPickle.dump([all_stats,cfg], open(result_file_name, "wb"))
