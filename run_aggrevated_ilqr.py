#!/usr/bin/env python

#from gym.envs import make
from modular_rl import *
from modular_rl.core import run_policy_gradient_optimal_control
import argparse, sys, cPickle
from tabulate import tabulate
import shutil, os, logging
import gym
import random
from dynamics import cartpole_gym, pusher, helicopter, swimmer, striker, inverted_double_pendulum
from dynamics import half_cheetah
from dynamics import hopper
#from dynamics import acrobot
from dynamics import Swimmer_rlpy
from dynamics import reacher
from traj_opt import algorithm
from IPython import embed
import cPickle


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    update_argument_parser(parser, GENERAL_OPTIONS)
    parser.add_argument("--env",required=True)
    parser.add_argument("--agent",required=True)
    parser.add_argument("--T", type=int, required=True)
    parser.add_argument("--seed", type = int, default = 1000)

    args,_ = parser.parse_known_args([arg for arg in sys.argv[1:] if arg not in ('-h', '--help')])
    if args.env == "CartPole":
        env = cartpole_gym.CartPoleEnv()
    elif args.env == "InvertedDoublePendulumEnv":
        env = inverted_double_pendulum.Customized_InvertedDoublePendulumEnv()
    elif args.env == "Reacher":
        env = reacher.Customized_ReacherEnv()
    elif args.env == "Pusher":
        env = pusher.Customized_PusherEnv()
    elif args.env == "Helicopter_Hover":
        env = helicopter.Helicopter_Hover()
    elif args.env == "Helicopter_Funnel":
        env = helicopter.Helicopter_Funnel()
    elif args.env == "Swimmer":
        env = swimmer.Customized_SwimmerEnv()
        #env = Swimmer_rlpy.Swimmer()
    elif args.env == "HalfCheetah":
        env = half_cheetah.Customized_HalfCheetahEnv()
    elif args.env == "Hopper":
        env = hopper.Customized_HopperEnv()
    elif args.env == "Striker":
        env = striker.Customized_StrikerEnv()
    elif args.env == "Acrobot":
        env = acrobot.Customized_AcrobotEnv()

    #update dimension of obs and action:
    args.dx = env.observation_space.shape[0]
    args.du = env.action_space.shape[0]

    agent_ctor = get_agent_cls(args.agent)
    update_argument_parser(parser, agent_ctor.options)
    update_argument_parser(parser, algorithm.OC_OPTIONS)

    args = parser.parse_args()
    args.timestep_limit = args.T #fixed time horizon
    cfg = args.__dict__
    cfg["timestep_limit"] = int(cfg["timestep_limit"])
    cfg["T"] = int(cfg["T"])
    
    np.random.seed(args.seed)
    env.seed(args.seed)
    random.seed(args.seed)
    agent = agent_ctor(env.observation_space, env.action_space, cfg)
    OC_solver = algorithm.Algorithm(env, env.observation_space, env.action_space, cfg)
    print cfg

    COUNTER = 0
    def callback(stats, iter):
        global COUNTER
        COUNTER += 1
        # Print stats
        print "*********** Iteration %i ****************" % COUNTER
        print tabulate(filter(lambda (k,v) : np.asarray(v).size==1, stats.items())) #pylint: disable=W0110
        # Store to hdf5
        #if args.use_hdf:
        #    for (stat,val) in stats.items():
        #        if np.asarray(val).ndim==0:
        #            diagnostics[stat].append(val)
        #        else:
        #            assert val.ndim == 1
        #            diagnostics[stat].extend(val)
            #if args.snapshot_every and ((COUNTER % args.snapshot_every==0) or (COUNTER==args.n_iter)):
            #    hdf['/agent_snapshots/%0.4i'%COUNTER] = np.array(cPickle.dumps(agent,-1))


    all_stats = run_policy_gradient_optimal_control_alt(env, agent, OC_solver, callback=callback,
                usercfg = cfg)
    #cPickle.dump(agent, open("{}_{}_{}_{}".format(cfg["env"], cfg["agent"], cfg["gamma"],cfg["lam"]),"wb"))
    result_file_name = "results/{}_{}_{}".format(cfg["env"], args.seed, "aggrevated_ilqr")
    cPickle.dump([all_stats, cfg], open(result_file_name, "wb"))





