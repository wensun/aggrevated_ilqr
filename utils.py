import numpy as np


def Jacobian_finite_difference(f, x, delta = 0.00123):
    x_next = f(x)
    A = np.zeros((x_next.shape[0], x.shape[0]))
    for i in xrange(x.shape[0]):
        x_p = x.copy()
        x_p[i] += delta
        x_next_p = f(x_p)
        x_m = x.copy()
        x_m[i] -= delta
        x_next_m = f(x_m)
        A[:,i] = (x_next_p - x_next_m)/(2.*delta)
    
    return A


def finite_difference(f, x, a, delta = 0.00123):
    #compute jacobians:
    x_next = f(x,a)
    x_next = np.array(x_next)
    if np.ndim(x_next) == 0:
        A = np.zeros((1, x.shape[0]))
        B = np.zeros((1, a.shape[0])) 
    else:
        A = np.zeros((x_next.shape[0], x.shape[0]))
        B = np.zeros((x_next.shape[0], a.shape[0]))
    #compute one dim by one dim:
    for i in xrange(x.shape[0]):
        x_p = x.copy()
        x_p[i] += delta
        x_next_p = f(x_p, a)
        x_m = x.copy()
        x_m[i] -= delta
        x_next_m = f(x_m, a)
        A[:,i] = (x_next_p - x_next_m)/(2.*delta)

    for i in xrange(a.shape[0]):
        a_p = a.copy()
        a_p[i] += delta
        x_next_p = f(x, a_p)
        a_m = a.copy()
        a_m[i] -= delta
        x_next_m = f(x, a_m)
        B[:,i] = (x_next_p - x_next_m)/(2.*delta)
    
    if np.ndim(x_next) == 0:
        return A[0],B[0]
    else:
        return A,B

def Hessian_finite_difference(f, x, a, delta = 0.00123):
    def new_f(xa):
        return f(xa[0:x.shape[0]], xa[x.shape[0]:])

    xa = np.concatenate([x,a],axis=0)
    H = np.zeros((xa.shape[0],xa.shape[0]))
    for i in range(xa.shape[0]):
        for j in range(i, xa.shape[0]):
            xa_pp = xa.copy()
            xa_pp[i] += delta
            xa_pp[j] += delta
            xa_mm = xa.copy()
            xa_mm[i] -= delta
            xa_mm[j] -= delta
            xa_pm = xa.copy()
            xa_pm[i] += delta
            xa_pm[j] -= delta
            xa_mp = xa.copy()
            xa_mp[i] -= delta
            xa_mp[j] += delta

            H[i,j] = ((new_f(xa_pp) - new_f(xa_mp)) - (new_f(xa_pm) - new_f(xa_mm)))/(4*delta**2)
            H[j,i] = H[i,j]
    
    return H[0:x.shape[0],0:x.shape[0]], H[x.shape[0]:xa.shape[0],x.shape[0]:xa.shape[0]], H[x.shape[0]:xa.shape[0], 0:x.shape[0]]


def linearize_dynamics(f, x, a, delta = 0.00123):
    A,B = finite_difference(f, x, a, delta)
    b = f(x,a) - A.dot(x) - B.dot(a)

    return A,B,b

def quadratize(c, x, a, delta = 0.00123):
    C_xx, C_uu, C_ux = Hessian_finite_difference(c, x,a)
    C_x, C_u = finite_difference(c, x, a)
    C = c(x,a) - C_x.dot(x) - C_u.dot(a) + 0.5*x.dot(C_xx).dot(x) + 0.5*a.dot(C_uu).dot(a) + a.dot(C_ux).dot(x)
    C_x = C_x - C_ux.transpose().dot(a) - C_xx.dot(x)
    C_u = C_u - C_ux.dot(x) - C_uu.dot(a)

    return C_xx, C_uu, C_ux, C_x, C_u, C

